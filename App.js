
import React from 'react';

import WelcomeScreen from './components/WelcomeScreen';
import Login from './components/Login';
import SignUp from './components/SignUp';
import AfterSplash from './components/AfterSplash'
import Home from './components/Home'

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { SafeAreaConsumer } from 'react-native-safe-area-context';
import PersonalDetails from './components/PersonalDetails'
import BasicAndLifeStyle from './components/BasicAndLifeStyle'
import ReligiousBackground from './components/ReligiousBackground'
import AstroDetails from './components/AstroDetails'


import ForgotPassword from './components/ForgotPassword'
import FamilyDetails from './components/FamilyDetails'
import EducationCareer from './components/EducationCareer'
import LocationGroom from './components/LocationGroom'
import UserUploadPhoto from './components/UserUploadPhoto'

import PartnerBasicInfo from './components/partner/PartnerBasicInfo'
import PartnerLocationInfo from './components/partner/PartnerLocationInfo'
import PartnerEducationCareer from './components/partner/PartnerEducationCareer'
import PartnerOtherDetails from './components/partner/PartnerOtherDetails'
import MainPage from './components/MainPage'
import ViewProfileDetails from './components/ViewProfileDetails' 
import Membership from './components/MemberShip/Membership'
import Membership_Plan from './components/MemberShip/Membership_Plan'
import Page1 from './components/Page1';
import Delete from './components/Delete';
import Setting from './components/Setting';
import HideProfile from './components/HideProfile';
import Search from './components/Search';

const Stack = createNativeStackNavigator();

const App = () =>  {
 
  return (
    <NavigationContainer>
    
      <Stack.Navigator initialRouteName="WelcomeScreen">
        
        
        <Stack.Screen name="WelcomeScreen" component={WelcomeScreen} options={{ headerShown: false }} />
        <Stack.Screen name="SignUp" component={SignUp} options={{ headerShown: false }} />
        <Stack.Screen name='Home' component={Home} options={{headerShown:false}}/>
        <Stack.Screen name="AstroDetails" component={AstroDetails} options={{headerShown:false}}/>
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="ViewProfileDetails" component={ViewProfileDetails} options={{ headerShown: false }} />
        <Stack.Screen name="AfterSplash" component={AfterSplash} options={{headerShown:false}}/>
        <Stack.Screen name="PersonalDetails" component={PersonalDetails} options={{headerShown:false}}/>
        <Stack.Screen name="BasicAndLifeStyle" component={BasicAndLifeStyle} options={{headerShown:false}}/>
        <Stack.Screen name="ReligiousBackground" component={ReligiousBackground} options={{headerShown:false}}/>
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{headerShown:false}}/>
        <Stack.Screen name="FamilyDetails" component={FamilyDetails} options={{headerShown:false}}/>
        <Stack.Screen name="EducationCareer" component={EducationCareer} options={{headerShown:false}}/>
        <Stack.Screen name="LocationGroom" component={LocationGroom} options={{headerShown:false}}/>
        <Stack.Screen name="UserUploadPhoto" component={UserUploadPhoto} options={{headerShown:false}}/>
        <Stack.Screen name="MainPage" component={MainPage} options={{headerShown:false}}/>

        <Stack.Screen name="Membership" component={Membership} options={{headerShown:false}}/>
        <Stack.Screen name="PartnerBasicInfo" component={PartnerBasicInfo} options={{headerShown:false}}/>
        <Stack.Screen name="PartnerLocationInfo" component={PartnerLocationInfo} options={{headerShown:false}}/>
        <Stack.Screen name="PartnerEducationCareer" component={PartnerEducationCareer} options={{headerShown:false}}/>
        <Stack.Screen name="PartnerOtherDetails" component={PartnerOtherDetails} options={{headerShown:false}}/>
        <Stack.Screen name="Membership_Plan" component={Membership_Plan} options={{headerShown:false}}/>
        <Stack.Screen name="Page1" component={Page1} options={{headerShown:false}}/>
        <Stack.Screen name="Delete" component={Delete} options={{headerShown:false}}/>
        <Stack.Screen name="Setting" component={Setting} options={{headerShown:false}}/>
        <Stack.Screen name="HideProfile" component={HideProfile} options={{headerShown:false}}/>
        <Stack.Screen name="Search" component={Search} options={{headerShown:false}}/>
      </Stack.Navigator>

      
    </NavigationContainer>
  );
};


export default App;
