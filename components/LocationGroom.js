
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React,{ Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';


import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'
import Spinner from 'react-native-loading-spinner-overlay';

import axios from 'axios';

const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class LocationGroom extends Component {

    constructor(props) {
        super(props);
        this.state = {
            countrisObject:{},
            countriesList:[],
            country:'',

            stateObject:{},
            stateList:[],
            state:'',

            cityList:[],
            city:'',

            pinCode:'',
            
            spinner:false,

            user_id:'',
            token:'',



        }
    }

    async componentDidMount() {

        let isComplete = await userProfileApi._profileStatus('location');

        if(isComplete==='success'){
            this.props.navigation.navigate('PartnerBasicInfo')
        }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        let objectss = await userProfileApi._countries();
        this.setState({countrisObject:objectss});
        
        let list = [];
         objectss.forEach(element => {
                //console.log(element.name);
                list.push(element.name);
        });

        //console.log('objectss ',list);
        this.setState({countriesList:list});


    
    }

    async onSelectCountry(index,val){
        //console.log(this.state.countrisObject[index].id);

        this.setState({country:val});

        this.setState({spinner:true})
        let st = await userProfileApi._state(this.state.countrisObject[index].id)
        this.setState({stateObject:st});

        let list = [];
        st.forEach(element => {
               //console.log(element.name);
               list.push(element.name);
       });
        // console.log('state ',st);
        this.setState({stateList:list});

        this.setState({spinner:false})

    }

    async onSelectState(index,val){
        this.setState({state:val});

        //console.log('state id',this.state.stateObject[index].id);
        this.setState({spinner:true})
        let ct = await userProfileApi._city(this.state.stateObject[index].id);

        let list = [];
        ct.forEach(element => {
               //console.log(element.name);
               list.push(element.name);
       });
        // console.log('state ',st);
        this.setState({cityList:list});

        this.setState({spinner:false})

    }

    async onSubmit(){

        // console.log(this.state.country+"  "+this.state.state+"  "+this.state.city);

        // const formData = new FormData();
        // formData.append('detail_type', 'location');
        // formData.append('user_id', this.state.user_id);
        // formData.append('current_city', this.state.country);
        // formData.append('state', this.state.state);
        // formData.append('residence_status', this.state.city);
        // formData.append('pin_code', this.state.pinCode);
       
        // await userProfileApi._locationAndGroom(formData);

        if(
            this.state.country !='' &&
            this.state.state !='' &&
            this.state.city  !='' &&
            this.state.pinCode !='' 
        ){

        
        await userProfileApi._locationAndGroom(this.state.user_id,
            this.state.country,this.state.state,this.state.city,this.state.pinCode);

        this.props.navigation.navigate('PartnerBasicInfo');
        }
    }

    render() {
       
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />


                    <Spinner
                        visible={this.state.spinner}
                        cancelable={true}


                    />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Location of Groom/Bride</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Current Residence's City *</Text>

                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.countriesList}
                            defaultValue={'Select Country'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 10,
                                height:300,
                                borderColor:COLORS.appColor,
                                borderWidth:1
                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.onSelectCountry(index,val)
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                       

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            State of Residence's *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.stateList}
                            defaultValue={'Select State'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                height:300,
                                borderColor:COLORS.appColor,
                                borderWidth:1

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.onSelectState(index,val)
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10 , fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Residence's Status *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.cityList}
                            defaultValue={'Select City'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor,

                                // marginLeft: -100
                            }}

                            onSelect={(index, val) =>
                                this.setState({ city: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Pin Code *</Text>
                            <TextInput
                        style={{
                            marginTop: 10,
                            borderColor: '#000',
                            borderWidth: 1,
                            borderRadius: 8,
                            padding: 5,
                            paddingLeft: 10

                        }}
                        value={this.state.pinCode}
                        keyboardType='number-pad'
                        placeholder='Type Here'
                        onChangeText={(text) => this.setState({ pinCode: text })}
                    />

                       
                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                    // onPress={()=>this.props.navigation.navigate('UserUploadPhoto')}
                    onPress={()=> this.onSubmit()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default LocationGroom;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})