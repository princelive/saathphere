
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;

class FamilyDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {

            father_status: '',
            mother_status: '',
            family_location: '',
            native_place: '',
            no_of_brothers: '0',
            no_of_sisters: '0',
            family_values: '',
            family_affuence: '',

            familyValuesList: [],
            familyInfluenceList: [],

            token: '',
            user_id: ''


        }
    }


    async componentDidMount() {

        let isComplete = await userProfileApi._profileStatus('family');

        if(isComplete==='success'){
            this.props.navigation.navigate('EducationCareer')
        }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        // console.log("Auth token ",token);


        let lang = await userProfileApi._familyValues();
        this.setState({ familyValuesList: lang });

        let influence = await userProfileApi._familyInfluence();
        this.setState({ familyInfluenceList: influence });

        // console.log('influence',influence);



    }

    async sendFamilyDetails() {

        if (this.state.father_status != '' && this.state.mother_status != '' && this.state.family_location != '' &&

            this.state.family_values != '' && this.state.native_place != '' &&
            this.state.no_of_brothers != '' && this.state.no_of_sisters != '' && this.state.family_affiliunce != '') {

            await userProfileApi._familyDetails(this.state.user_id, this.state.father_status,
                this.state.mother_status, this.state.family_location, this.state.native_place,
                this.state.no_of_brothers, this.state.no_of_sisters,
                this.state.family_values, this.state.family_affiliunce);


                this.props.navigation.navigate('EducationCareer');

        }


       

    }


    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Family Details</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Father Status *</Text>


                        <TextInput
                            value={this.state.father_status}
                            onChangeText={(text) => this.setState({ father_status: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Mother Status *</Text>
                        <TextInput
                            value={this.state.mother_status}
                            onChangeText={(text) => this.setState({ mother_status: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Family Location *</Text>
                        <TextInput
                            value={this.state.family_location}
                            onChangeText={(text) => this.setState({ family_location: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Native Place *</Text>
                        <TextInput
                            value={this.state.native_place}
                            onChangeText={(text) => this.setState({ native_place: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />









                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            No. Of Brothers *</Text>
                        <TextInput
                            value={this.state.no_of_brothers}
                            onChangeText={(text) => this.setState({ no_of_brothers: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter'
                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            No Of Sisters *</Text>
                        <TextInput
                            value={this.state.no_of_sisters}
                            onChangeText={(text) => this.setState({ no_of_sisters: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter '
                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Family Values *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.familyValuesList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ family_values: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Family Affluence *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.familyInfluenceList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ family_affuence: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('EducationCareer')}
                        onPress={() => this.sendFamilyDetails()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default FamilyDetails;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})