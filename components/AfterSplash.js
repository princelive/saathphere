
import React, { Component } from 'react';

import {
    ImageBackground,
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';



const image = require('../images/splash_background.jpg');



class AfterSplash extends Component {



    render() {

        return (
            <View style={{ flex: 1 }}>
                <StatusBar barStyle="light-content" backgroundColor="#d9475c" />
                <ImageBackground source={image} resizeMode="cover"
                    style={{ flex: 1, justifyContent: 'center' }}>

                    <View style={styles.transParentView}>

                        <View style={{ flexDirection: 'row', bottom: 60,
                         marginHorizontal: 30,
                        justifyContent:'center',
                       
                         }}>
                            <Pressable style={{
                                backgroundColor: '#fff',
                                width:150,
                                justifyContent: 'center',
                                paddingTop: 10,
                                paddingBottom: 10,
                                paddingRight: 0,
                                borderRadius: 8,
                                marginRight:10
                            }}
                            onPress={()=>this.props.navigation.navigate("Login")}
                            >
                                <Text style={styles.btn}>Login</Text>
                            </Pressable>

                            <Pressable style={{
                                backgroundColor: '#fff',
                                width:150,
                                justifyContent: 'center',
                                paddingTop: 10,
                                paddingBottom: 10,
                                paddingRight: 0,
                                borderRadius: 8,
                                marginLeft:10
                            }}
                            onPress={()=>this.props.navigation.navigate("SignUp")}
                            >
                                <Text style={styles.btn}>Registration</Text>
                            </Pressable>
                        </View>
                    </View>

                </ImageBackground>
            </View>
        );
    };
}


export default AfterSplash;

const styles = StyleSheet.create({
    transParentView: {
        flex: 1,
        // backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'flex-end',
        
    },
    btn: {

        color: '#d9475c',
        alignSelf: 'center',
        fontSize:15,
        fontWeight:'bold'
        
    }
})
