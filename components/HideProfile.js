
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';


import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class HideProfile extends Component {

    constructor(props) {
        super(props);
        this.state = {
           clickYes:false

        }
    }

    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('education');

        // if(isComplete==='success'){
        //     this.props.navigation.navigate('Home')
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });


    }

    async onSubmit() {
        //alert(this.state.createdBy+"  "+this.state.diet)

        await userProfileApi._other_details(this.state.user_id, this.state.createdBy, this.state.diet);
    }

    onLayout2open(){
    this.setState({clickYes:true})
    }

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',

                           }
                            }
                        >Hide Profile</Text>
                    </View>
{this.renderif(!this.state.clickYes)(
                    <View>
                        <Text
                            style={{
                                color: '#000',
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 30,
                                marginTop: 30


                            }
                            }
                        >Are You Sure \n You Want to Delete Your Profile?</Text>

                        <View style={{ marginHorizontal: 20, marginTop: 40, flexDirection: 'row', justifyContent: 'space-between' }}>

                            <Pressable style={{
                                backgroundColor: '#d9475c',
                                justifyContent: 'center',
                                marginTop: 40,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 7,
                                elevation: 5,
                                marginHorizontal: 40,
                                marginBottom: 20,
                                width: 40,
                                height: 30
                            }}
                                // onPress={() => this.props.navigation.navigate('')}
                                onPress={() => this.onLayout2open()}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'center',
                                        color: '#fff',
                                        fontSize: 15
                                    }}
                                >7 Days</Text>
                            </Pressable>

                            <Pressable style={{
                                backgroundColor: '#d9475c',
                                justifyContent: 'center',
                                marginTop: 40,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 7,
                                elevation: 5,
                                marginHorizontal: 40,
                                marginBottom: 20,
                                width: 40,
                                height: 30

                            }}
                                onPress={() => this.props.navigation.goBack()}
                            // onPress={() => this.props.navigation.navigate('')}
                            // onPress={()=> this.onSubmit()}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'center',
                                        color: '#fff',
                                        fontSize: 15
                                    }}
                                >15 Days</Text>
                            </Pressable>

                            <Pressable style={{
                                backgroundColor: '#d9475c',
                                justifyContent: 'center',
                                marginTop: 40,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 7,
                                elevation: 5,
                                marginHorizontal: 40,
                                marginBottom: 20,
                                width: 40,
                                height: 30

                            }}
                                onPress={() => this.props.navigation.goBack()}
                            // onPress={() => this.props.navigation.navigate('')}
                            // onPress={()=> this.onSubmit()}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'center',
                                        color: '#fff',
                                        fontSize: 15
                                    }}
                                >1 Month</Text>
                            </Pressable>

                        </View>
                    </View>
)}

{this.renderif(this.state.clickYes)(
                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>

                        <Text style={{ fontSize: 20, color: '#000' }}> Why You Want to Delete Your Profile</Text>

                        <TextInput
                            value={this.state.createdBy}
                            onChangeText={(text) => this.setState({ createdBy: text })}
                            style={{
                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 10,
                                height: 100,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Write Here.........'
                        />


                        <Pressable style={{
                            backgroundColor: '#d9475c',
                            justifyContent: 'center',
                            marginTop: 40,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 7,
                            elevation: 5,
                            marginHorizontal: 40,
                            marginBottom: 20
                        }}
                            // onPress={() => this.props.navigation.navigate('')}
                            onPress={() => this.onSubmit()}
                        >
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 15
                                }}
                            >Delete</Text>
                        </Pressable>

                    </View>
)}

                </View>
            </ScrollView>
        );
    };
}



export default HideProfile;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})