import React, { Component } from 'react';
import { ToastAndroid } from 'react-native';
import axios from 'axios';
import mAppConstant from './utils/AppConstant';
import Spinner from 'react-native-loading-spinner-overlay';
import {
    Pressable,
    TouchableOpacity,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    useColorScheme,
    View,
} from 'react-native';
import { SocialIcon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome5';

class ForgotPassword extends Component{
    constructor(props) {
        super(props);

        this.state = {
            spinner: false
        }
    }
    render() {

        return (
            <View style={{ flex: 1, backgroundColor: '#f6f7fb' }}>
                <StatusBar barStyle="light-content" backgroundColor="#d9475c" />

                <Text
                    style={{
                        textAlign: 'center',
                        marginTop: 40,
                        color: '#d9475c',
                        fontSize: 25
                    }}
                >
                    Forgot Password
                </Text>

                <View style={{
                    marginTop: 30,
                    marginHorizontal: 30

                }}>

                    <Text style={{ color: '#000', fontWeight: 'bold' }}>Enter OTP to change Your Password</Text>
                    <TextInput
                        style={{
                            marginTop: 10,
                            borderColor: '#000',
                            borderWidth: 1,
                            borderRadius: 8,
                            padding: 5,
                            paddingLeft: 10

                        }}

                        placeholder='................'
                        onChangeText={text => this.setState({ textEmail: text })}
                    />

                    <Text style={{ color: '#000', fontWeight: 'bold', marginTop: 20 }}>Password</Text>

                    <View style={{
                        marginTop: 10,
                        borderColor: '#000',
                        borderWidth: 1,
                        borderRadius: 8,
                        justifyContent: 'space-between',
                        height: 40,
                        flexDirection: 'row',

                    }}>
                        <TextInput
                            style={{

                                paddingLeft: 10,
                                alignSelf: 'flex-start'

                            }}
                        
                            placeholder='Enter your Password'
                            onChangeText={text => this.setState({ textPassword: text })}
                        />

                        <Icon
                          //  name={hidePass ? 'eye-slash' : 'eye'}
                            size={18}
                            color="#d9475c"
                            //onPress={() => this.setState({ hidePass: !hidePass })}
                            style={{
                                textAlignVertical: 'center',
                                marginRight: 13
                            }}
                        />
                    </View>
                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5
                    }} onPress={()=>this._submitLogin()}>
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 18
                            }}
                        >Submit</Text>
                    </Pressable>

                    <Text
                        style={{
                            marginTop: 15,
                            color: '#d9475c',
                            fontWeight: 'bold',
                            alignSelf: 'center'
                        }}
                    >
                       Resend OTP ?
                    </Text>

                  
                </View>


            </View>
        );
    };
}
export default ForgotPassword;

const styles = StyleSheet.create({
    transParentView: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'flex-end',

    },
    horizontal: {
        borderTopColor: '#97999d',
        width: 150,
        borderTopWidth: 1,
        height: 0,
        marginTop: 9
    }

})