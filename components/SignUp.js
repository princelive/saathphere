
import React from 'react';

import {
  Pressable,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
// import Icon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import userApi from '../Api/User_Api'
import { Button, Overlay } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import ModalDropdown from 'react-native-modal-dropdown';
import AntDesign from 'react-native-vector-icons/AntDesign'


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;

class SignUp extends Component {

  constructor(props) {
    super(props);

    this.state = {
      country: 'Select',
      phnoDrpDwn: '+91',
      hidePass: true,
      phoneVerifyOverlay: false,
      email: '',
      phoneNumber: '',
      otpPhn: '',
      password: "",
      cnf_password: '',
      spinner: false,
      otp: '',
      otpResp: '',
      userName: '',
      verified: '',
      genderName:'',
      gender: '',

    }
  }

  async componentDidMount() {
    let profileFor = await userApi.getProfileType();
    //console.log(profileFor.response);
  }

  async verifyPhone() {
    this.setState({ spinner: true });
    let res = await userApi.verifyOtp(this.state.phoneNumber, this.state.otp);
    this.setState({ spinner: false });
    this.setState({ phoneVerifyOverlay: false });
    if (res == 'verified') {
      this.setState({ verified: res })
      //alert('hiii');
      // this.setState({ spinner: true });
      // await userApi.registration(this.state.userName,this.state.email,this.state.password,this.state.cnf_password,this.state.phoneNumber);

      //await this.callRegistration();
      //  this.setState({ spinner: false });
    }
  }

  async sendOtp() {

    if (this.state.phoneNumber != '' || this.state.gender != '') {

      if (this.state.verified === 'verified') {
        // alert('sending');
        this.setState({ spinner: true });
        await userApi._registration(this.state.userName, this.state.email, this.state.password, this.state.cnf_password, this.state.phoneNumber, this.state.gender);
        this.setState({ spinner: false });
        this.props.navigation.navigate('Login')

      } else {
        // alert(this.state.verified);
        this.setState({ spinner: true });

        let res = await userApi.sendOtp(this.state.phoneNumber);
        this.setState({ spinner: false });
        // alert(res);
        this.setState({ otpResp: res });

        this.setState({ phoneVerifyOverlay: true });
        // console.log(res)

      }
    }

  }

  async callRegistration() {
    // alert('hiiii');
    //userApi.registration(this.state.userName, this.state.email, this.state.password, this.state.cnf_password, this.state.phoneNumber);
    //alert(res);
    // await userApi.banner_call();
    await userApi._registration(this.state.userName, this.state.email, this.state.password, this.state.cnf_password, this.state.phoneNumber, this.state.gender);
    this.props.navigation.navigation('Login')
    //console.log("abc "+this.state.userName+" "+this.state.phoneNumber+"  "+this.state.email+" -"+this.state.password+" - "+this.state.phoneNumber);

  }

  render() {
    const { hidePass } = this.state
    return (
      <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
        <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
          <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

          <Text
            style={{
              color: COLORS.appColor,
              marginTop: 50,
              textAlign: 'center',
              fontSize: 24
            }
            }
          >Register</Text>

          {/* ********************* phone Verify Overlay start **************** */}
          <Overlay isVisible={this.state.phoneVerifyOverlay}>
            <View style={{ width: 350, paddingTop: 10, paddingBottom: 20 }}>

              <Text style={{ fontSize: 18, textAlign: 'center', marginTop: 17 }}>Verify Mobile Number!</Text>
              <TextInput
                style={{
                  height: 40,
                  borderWidth: 1,
                  marginTop: 22,
                  borderRadius: 5,
                  marginHorizontal: 30,
                  paddingLeft: 10
                }}
                placeholder="Enter --OTP--"
                keyboardType='number-pad'
                secureTextEntry={true}
                value={this.state.otp}
                onChangeText={(text) => this.setState({ otp: text })}
              />

              <Text style={{ fontSize: 10, textAlign: 'center', marginTop: 7 }}>An OTP has been sent to your Mobile Number!</Text>

              <Pressable style={{
                backgroundColor: '#d9475c',
                justifyContent: 'center',
                marginTop: 20,
                paddingTop: 10,
                paddingBottom: 10,
                borderRadius: 7,
                elevation: 5,
                marginHorizontal: 40
              }}
                onPress={() => this.verifyPhone()}
              >
                <Text
                  style={{
                    alignSelf: 'center',
                    color: '#fff',
                    fontSize: 15

                  }}
                >Verify</Text>
              </Pressable>
            </View>
          </Overlay>



          {/* *********************** Overlay End  Spinner Start********************* */}


          <Spinner
            visible={this.state.spinner}
            cancelable={true}


          />


          {/* ************* Spinner End ************************ */}
          <View style={{ marginHorizontal: 40, marginTop: 40 }}>
            <Text>Create Profile For</Text>

            <DropDownPicker
              items={[
                { label: 'Select', value: 'Select' },
                { label: 'sister', value: 'sister', },
                { label: 'brother', value: 'brother', },
                { label: 'Friends', value: 'Friends', },
              ]}
              defaultValue={this.state.country}
              containerStyle={{ height: 40, marginTop: 8 }}
              style={Styles.dropDownStyles}
              itemStyle={{
                justifyContent: 'flex-start',

              }}
              dropDownStyle={{ backgroundColor: '#fff' }}
              onChangeItem={item => this.setState({
                userName: item.value
              })}
            />
          </View>

          {/* <Text style={{ marginHorizontal: 40, marginTop: 25 }}>User Name</Text>
          <TextInput
            style={{
              borderColor: '#000',
              marginHorizontal: 40,
              borderColor: '#000',
              borderWidth: 1,
              marginTop: 8,
              height: 40,
              borderRadius: 5,
              paddingLeft: 15

            }}
            placeholder='Enter user name'
            value={this.state.userName}
            onChangeText={(text) => this.setState({ userName: text })}
          /> */}

          <Text style={{ marginHorizontal: 40, marginTop: 25 }}>Email Address</Text>
          <TextInput
            style={{
              borderColor: '#000',
              marginHorizontal: 40,
              borderColor: '#000',
              borderWidth: 1,
              marginTop: 8,
              height: 40,
              borderRadius: 5,
              paddingLeft: 15

            }}
            placeholder='username@gmail.com'
            value={this.state.email}
            onChangeText={(text) => this.setState({ email: text })}
          />

          <View style={{ marginHorizontal: 40, marginTop: 25 }}>
            <Text>Phone Number</Text>
            <View style={{
              flexDirection: 'row',
              borderColor: '#000',
              borderWidth: 1,
              height: 40,
              borderRadius: 5,
              marginTop: 7,
            }}>
              <DropDownPicker
                items={[
                  { label: '+91', value: '+91' },
                  // { label: '04', value: '04', },
                  // { label: '+56', value: '+56', },
                ]}
                defaultValue={this.state.phnoDrpDwn}
                containerStyle={{ height: 38, width: 70 }}
                style={Styles.phnDropDown}
                itemStyle={{
                  justifyContent: 'flex-start',

                }}
                dropDownStyle={{ backgroundColor: '#fff' }}
                onChangeItem={item => this.setState({
                  phnoDrpDwn: item.value
                })}
              />
              <TextInput
                style={{
                  height: 40,
                  // fontSize: 1,
                  alignSelf: 'stretch',
                  paddingLeft: 7,
                  width: 250,
                }}
                keyboardType="numeric"
                value={this.state.phoneNumber}
                onChangeText={(text) => this.setState({ phoneNumber: text })}
                placeholder='Enter Phone Number'
              />
            </View>

            <View style={{ marginHorizontal: 0, marginTop: 20 }}>
              <Text>Gender</Text>
              <DropDownPicker
                items={[
                  { label: 'Select', value: 'Select' },
                  { label: 'Male', value: 'Male', },
                  { label: 'Female', value: 'Female', },

                ]}
                defaultValue={this.state.country}
                containerStyle={{ height: 40, marginTop: 8 }}
                style={Styles.dropDownStyles}
                itemStyle={{
                  justifyContent: 'flex-start',

                }}
                dropDownStyle={{ backgroundColor: '#fff' }}
                onChangeItem={item => this.setState({
                  gender: item.value
                })}
              />
            </View>

          </View>

          <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 40 }}>
            Password</Text>

          <View style={{
            marginTop: 7,
            borderColor: '#000',
            borderWidth: 1,
            borderRadius: 5,
            justifyContent: 'space-between',
            height: 40,
            marginHorizontal: 40,
            flexDirection: 'row',

          }}>
            <TextInput
              style={{

                paddingLeft: 10,
                alignSelf: 'flex-start',
                width: 270,
              }}
              secureTextEntry={hidePass ? true : false}
              placeholder='Enter your Password'
              value={this.state.password}
              onChangeText={(text) => this.setState({ password: text })}
            />

            <Icon
              name={hidePass ? 'eye-slash' : 'eye'}
              size={18}
              color="#d9475c"
              onPress={() => this.setState({ hidePass: !hidePass })}
              style={{
                textAlignVertical: 'center',
                marginRight: 13

              }}
            />
          </View>

          <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 40 }}>
            Confirm Password</Text>
          <View style={{
            marginTop: 7,
            borderColor: '#000',
            borderWidth: 1,
            borderRadius: 5,
            justifyContent: 'space-between',
            height: 40,
            marginHorizontal: 40,
            flexDirection: 'row',

           }}>
            <TextInput
              style={{

                paddingLeft: 10,
                alignSelf: 'flex-start',
                width: 270,
              }}
              secureTextEntry={hidePass ? true : false}
              placeholder='Confirm Password'
              value={this.state.cnf_password}
              onChangeText={(text) => this.setState({ cnf_password: text })}
            />

            <Icon
              name={hidePass ? 'eye-slash' : 'eye'}
              size={18}
              color="#d9475c"
              onPress={() => this.setState({ hidePass: !hidePass })}
              style={{
                textAlignVertical: 'center',
                marginRight: 13,
              }}
            />
          </View>

          {/* <View style={{ marginHorizontal: 40, marginTop: 30 }}>
            <Text>Gender</Text>

            <DropDownPicker
              items={[
                { label: 'Select Gender', value: 'Select' },
                { label: 'Male', value: 'Male', },
                { label: 'Female', value: 'Female', },
              ]}
              defaultValue={this.state.country}
              containerStyle={{ height: 40, marginTop: 8 }}
              style={Styles.dropDownStyles}
              itemStyle={{
                justifyContent: 'flex-start',

              }}
              dropDownStyle={{ backgroundColor: '#fff' }}
              onChangeItem={item => this.setState({
                country: item.value
              })}
            />
          </View> */}

          <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 40 }}>
            Gender</Text>
          <ModalDropdown

            animated={true}
            isFullWidth={true}
            options={[
              'Male', 'Female'
            ]}
            defaultValue={'Select Or Type'}

            containerStyle={{ height: 40, marginTop: 8 }}
            style={Styles.dropDownStyles1}

            dropdownStyle={{
              backgroundColor: '#fff', marginTop: 10,

              // marginLeft: -100
            }}


            onSelect={(index, val) =>
              this.setState({ genderName: val })
            }
            dropdownTextStyle={{
              color: '#000'
            }}
            textStyle={{ marginLeft: 10 }}
            renderRightComponent={() => (
              <AntDesign
                name={'down'}
                size={18}
                color="#000"
                style={{ marginLeft: 'auto', marginRight: 10 }}
              />
            )}

          />

          <Pressable style={{
            backgroundColor: '#d9475c',
            justifyContent: 'center',
            marginTop: 40,
            paddingTop: 10,
            paddingBottom: 10,
            borderRadius: 7,
            elevation: 5,
            marginHorizontal: 40
          }}
             onPress={() => this.callRegistration()}

           // onPress={() => this.sendOtp()}
          >
            <Text
              style={{
                alignSelf: 'center',
                color: '#fff',
                fontSize: 15

              }}
            >Register Free</Text>
          </Pressable>


          <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 60 }}>

            <Text>Already have an account? </Text>

            <Text style={{
              color: '#d9475c',
              fontWeight: 'bold',
            }}
              onPress={() => this.props.navigation.navigate('Login')}
            >
              Login
            </Text>


          </View>
          <View
            style={{

              borderTopColor: '#d9475c',
              width: 150,
              borderTopWidth: 1.5,
              height: 0,
              marginTop: 9,
              alignSelf: 'center'
            }}
          />
        </View>
      </ScrollView>
    );
  };
}



export default SignUp;

const Styles = StyleSheet.create({
  dropDownStyles: {
    backgroundColor: '#fff',
    borderColor: '#000'


  },
  phnDropDown: {
    backgroundColor: '#fff',


  },
  dropDownStyles1: {
    backgroundColor: '#fff',
    borderColor: '#000',
    height: 40,
    borderWidth: 1,
    borderRadius: 5,
    marginTop: 7,
    justifyContent: 'center',
    marginHorizontal: 40,
},
})