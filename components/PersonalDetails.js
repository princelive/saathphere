
import React from 'react';

import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import DatePicker from 'react-native-datepicker'
import SecureStorage from 'react-native-secure-storage'
import userProfileApi from '../Api/UserProfileApi';
import Spinner from 'react-native-loading-spinner-overlay';


const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;



const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class PersonalDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            spinner: false,
            country: 'Select',
            phnoDrpDwn: '+91',
            hidePass: true,
            groomName: '',
            dob: '',
            date: "",
            community: '',
            postedBy: '',
            location: '',
            maritalStatus: 0,
            religion: '',

            motherTanguesList: [],
            selected_mother_tounge: '',
            groomHeight: [],
            groom_Selected_Height: '',
            religionList: [],
            selected_religion: '',
            communityList: [],
            selected_community: '',
            maritalList: [],
            selected_marital: '',




            user_id: '',
            token: '',
        }
    }


    async componentDidMount() {

        let isComplete = await userProfileApi._profileStatus('personal');

        if(isComplete==='success'){
            this.props.navigation.navigate('BasicAndLifeStyle')
        }
        console.log('rrr',isComplete)

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        // console.log("Auth token ",token);


        let lang = await userProfileApi._motherTangues();
        //console.log('lang ',lang);
        this.setState({ motherTanguesList: lang });

        let hght = await userProfileApi._groomHeight();
        // console.log(hght);
        this.setState({ groomHeight: hght });

        let relgin = await userProfileApi._complete_profile_categories();
        this.setState({religionList:relgin});
       


    }

    setMaritalStatus(text){
        if(text==="Married"){
            this.setState({maritalStatus:1});
        }else{
            this.setState({maritalStatus:0});
        }
    }

    async setData() {
       
        this.setState({spinner:true});
       // alert('hiii');
        if(
            this.state.dob!='' &&
            this.state.groomName!='' &&
            this.state.groom_Selected_Height!='' &&
            this.state.religionList !='' &&
            this.state.community !='' &&
            this.state.maritalStatus !='' &&
            this.state.location !='' &&
            this.state.postedBy !='' &&
            this.state.selected_mother_tounge !='' 
        ){

        await userProfileApi._personal(this.state.user_id, this.state.groomName, this.state.dob,
            this.state.groom_Selected_Height, this.state.religion, this.state.community,
            this.state.maritalStatus, this.state.location, this.state.postedBy,
            this.state.selected_mother_tounge);

            this.props.navigation.navigate('BasicAndLifeStyle')

        }

        this.setState({spinner:false});

        
        
    }

    render() {
        const { hidePass } = this.state
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />


                    <Spinner
                        visible={this.state.spinner}
                        cancelable={true}


                    />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',
                            }
                            }
                        >Personal Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Personal Details</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Groom Name*</Text>

                        <TextInput
                            value={this.state.groomName}
                            onChangeText={(text) => this.setState({ groomName: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Date Of Birth*</Text>

                        <View style={{
                            marginTop: 7,
                            borderColor: '#000',

                            justifyContent: 'space-between',
                            height: 40,

                            flexDirection: 'row',

                        }}>
                            <DatePicker
                                style={Styles.datepicker}
                                date={this.state.dob}
                                mode="date"
                                placeholder="DOB"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    color: '#fff',
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36,

                                    },

                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(text) => { this.setState({ dob: text }) }}
                            />
                        </View>

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Height*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.groomHeight}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ groom_Selected_Height: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Religion*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.religionList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1,
                                height:90
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ religion: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Community*</Text>
                        {/* <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                                'option 1', 'option 2', 'option 3', 'option 4'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80,}}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor:'#f1f1f1', marginTop: 0,
                                borderColor:COLORS.appColor,
                                borderWidth:1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{fontSize:15}}
                        

                            onSelect={(index, val) =>
                                this.setState({ groomName: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15,
                                
                            }}
                            textStyle={{ marginLeft: 10 ,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        /> */}

                        <TextInput
                            value={this.state.community}
                            onChangeText={(text) => this.setState({ community: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Community'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Marital Status*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                                'Married', 'Not Married'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 30, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1,
                                height:100
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setMaritalStatus(val)
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Location*</Text>
                        {/* <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                                'option 1', 'option 2', 'option 3', 'option 4'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ groomName: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        /> */}

                        <TextInput
                            value={this.state.location}
                            onChangeText={(text) => this.setState({ location: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Community'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Posted By*</Text>
                        {/* <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                                'option 1', 'option 2', 'option 3', 'option 4'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ groomName: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}
                        /> */}

                        <TextInput
                            value={this.state.postedBy}
                            onChangeText={(text) => this.setState({ postedBy: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Community'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Mother Tangue*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.motherTanguesList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ selected_mother_tounge: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />
                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}

                        // onPress={() => this.props.navigation.navigate('BasicAndLifeStyle')}

                        onPress={() => this.setData()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>



                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default PersonalDetails;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    },
    datepicker: {
        flex: 1,
        height: 40,
        borderColor: '#b84127',

        marginTop: 10,

        width: width,
        backgroundColor: "#fff",
        // color:'#fff'

    },

})