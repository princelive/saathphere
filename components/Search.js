
import {
    Pressable,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import homeApi from '../Api/Home_Api';

const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class Search extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: ''

        }
    }


    async onSubmit() {
        await homeApi.deleteProfile(this.state.searchText);
    }

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <View style={{
                            borderColor: '#000',
                            borderWidth: 1, marginTop: 10,
                            borderRadius: 5,
                            paddingLeft: 15,
                            flexDirection:'row'
                        }}>
                        <Icon style={{ marginTop: 13}} name="ios-search" size={20} color="#000" />
                        <TextInput
                            onChangeText={(text) => this.setState({ searchText: text })}
                            style={{
                                color: '#000',
                           }}
                            placeholder='Enter your Keyword'
                        />
</View>
                        <Pressable style={{
                            backgroundColor: '#d9475c',
                            justifyContent: 'center',
                            marginTop: 40,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 7,
                            elevation: 5,
                            marginHorizontal: 40,
                            marginBottom: 20
                        }}
                            onPress={() => this.onSubmit()}
                        >
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 15
                                }}
                            >Search</Text>
                        </Pressable>

                    </View>


                </View>
            </ScrollView>
        );
    };
}



export default Search;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})