
import React, { Component } from 'react';

import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    TextInput,
    useColorScheme,
    View,
} from 'react-native';

import { SocialIcon } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome5';
import userApi from '../Api/User_Api'
import { COLORS } from '../styles/COLOR';
import { Button, Overlay } from 'react-native-elements';
import Spinner from 'react-native-loading-spinner-overlay';
import SecureStorage from 'react-native-secure-storage'

import userProfileApi from '../Api/UserProfileApi';



class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            hidePass: true,
            email: '',
            password: '',
            phNumber: '',
            loginTypeEmail: true,
            phoneVerifyOverlay: false,
            spinner: false,
            otp: '',
            loginError: '',
            loginResponse: ''
        }
    }


    async navigateScreen() {
        //let personal = '';
        let isComplete = await userProfileApi._profileStatus('personal');
        let basic = await userProfileApi._profileStatus('basic');
        let religion = await userProfileApi._profileStatus('religion');
        let astro = await userProfileApi._profileStatus('astro');
        let familyDetals = await userProfileApi._profileStatus('family');
        let eduAndCareer = await userProfileApi._profileStatus('education');
        let locatinogroom = await userProfileApi._profileStatus('location');
        let partnerBasic = await userProfileApi._profileStatus('partner-basic');
        let partnerLocation = await userProfileApi._profileStatus('partner_location');
        let partnerEdu = await userProfileApi._profileStatus('partner-education');
        let partnerOther = await userProfileApi._profileStatus('partner-other');



        //console.log(basic);
        if (isComplete === 'success') {
            if (basic === 'success') {
                if (religion === 'success') {
                    if (astro === 'success') {
                        if (familyDetals === 'success') {
                            if (eduAndCareer === 'success') {
                                if (locatinogroom == 'success') {
                                    if (partnerBasic == 'success') {
                                        if (partnerLocation === 'success') {
                                            if (partnerEdu === 'success') {
                                                if (partnerOther == 'success') {
                                                    this.props.navigation.navigate('MainPage')
                                                } else {
                                                    this.props.navigation.navigate('PartnerOtherDetails');
                                                }
                                            } else {
                                                this.props.navigation.navigate('PartnerEducationCareer')
                                            }
                                        } else {
                                            this.props.navigation.navigate('PartnerLocationInfo')
                                        }
                                    } else {
                                        this.props.navigation.navigate('PartnerBasicInfo');
                                    }
                                } else {
                                    this.props.navigation.navigate('LocationGroom');
                                }
                            } else {
                                this.props.navigation.navigate('EducationCareer');
                            }
                        } else {
                            this.props.navigation.navigate('FamilyDetails');
                        }
                    } else {
                        this.props.navigation.navigate('AstroDetails');
                    }
                } else {
                    this.props.navigation.navigate('ReligiousBackground');
                }

            } else {
                this.props.navigation.navigate('BasicAndLifeStyle');
            }
        } else {
            this.props.navigation.navigate('PersonalDetails')
        }


        // console.log('rrr',isComplete)
    }

    async userLogin() {

        this.setState({ spinner: true });
        let res = await userApi.login(this.state.email, this.state.password);

        //    console.log(res.data.token)
        //alert(res)
        if (res.success) {
            await SecureStorage.removeItem('token');
            await SecureStorage.removeItem('user_id');
            await SecureStorage.setItem('token', res.data.token);
            await SecureStorage.setItem('user_id', "" + res.data.user_id);
            // await SecureStorage.setItem('token')
            this.props.navigation.navigate('MainPage')
            // this.props.navigation.navigate('UserUploadPhoto')
            //await this.navigateScreen();
        } else {
            this.setState({ loginError: 'Invalid user name or password!' });
        }
        this.setState({ spinner: false });
    }


    async sendOtp() {

        console.log(this.state.phNumber);
        this.setState({ spinner: true });

        let res = await userApi.sendOtp(this.state.phNumber);
        this.setState({ spinner: false });
        // alert(res);
        //   this.setState({ otpResp: res });

        this.setState({ phoneVerifyOverlay: true });
        // console.log(res)

    }


    async verifyPhone() {

        this.setState({ spinner: true });
        let res = await userApi.verifyOtp(this.state.phNumber, this.state.otp);
        this.setState({ spinner: false });
        this.setState({ phoneVerifyOverlay: false });
        if (res == 'verified') {
            this.props.navigation.navigate('PersonalDetails')
        } else {
            this.setState({ spinner: false });
            this.setState({ loginError: 'Invalid phone number!' });
        }
    }

    render() {

        const { hidePass } = this.state
        return (
            <View style={{ flex: 1, backgroundColor: '#f6f7fb' }}>
                <StatusBar barStyle="light-content" backgroundColor="#d9475c" />

                <Text
                    style={{
                        textAlign: 'center',
                        marginTop: 20,
                        color: '#d9475c',
                        fontSize: 25
                    }}
                >
                    Login
                </Text>


                {/* ********************* phone Verify Overlay start **************** */}
                <Overlay isVisible={this.state.phoneVerifyOverlay}>
                    <View style={{ width: 350, paddingTop: 10, paddingBottom: 20 }}>

                        <Text style={{ fontSize: 18, textAlign: 'center', marginTop: 17 }}>Verify Mobile Number!</Text>
                        <TextInput
                            style={{
                                height: 40,
                                borderWidth: 1,
                                marginTop: 22,
                                borderRadius: 5,
                                marginHorizontal: 30,
                                paddingLeft: 10
                            }}
                            placeholder="Enter --OTP--"
                            keyboardType='number-pad'
                            secureTextEntry={true}
                            value={this.state.otp}
                            onChangeText={(text) => this.setState({ otp: text })}
                        />

                        <Text style={{ fontSize: 10, textAlign: 'center', marginTop: 7 }}>An OTP has been sent to your Mobile Number!</Text>

                        <Pressable style={{
                            backgroundColor: '#d9475c',
                            justifyContent: 'center',
                            marginTop: 20,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 7,
                            elevation: 5,
                            marginHorizontal: 40
                        }}
                            onPress={() => this.verifyPhone()}
                        >
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 15

                                }}
                            >Verify</Text>
                        </Pressable>
                    </View>
                </Overlay>



                {/* *********************** Overlay End  Spinner Start********************* */}


                <Spinner
                    visible={this.state.spinner}
                    cancelable={true}


                />


                <View style={{
                    flexDirection: 'row',

                    marginTop: 40,
                    marginHorizontal: 80

                }}>

                    <View style={{ marginRight: 10 }}>
                        <Text style={{ color: this.state.loginTypeEmail ? COLORS.appColor : '#000', alignSelf: 'center', fontWeight: '300' }}
                            onPress={() => this.setState({ loginTypeEmail: !this.state.loginTypeEmail })}
                        >Using Email</Text>
                        <View style={{ borderBottomColor: COLORS.appColor, borderBottomWidth: .5, marginTop: 7, width: 110 }} />
                    </View>

                    <View style={{ marginLeft: 10, marginLeft: 'auto' }}>
                        <Text style={{
                            color: this.state.loginTypeEmail ? '#000' : COLORS.appColor, alignSelf: 'center',
                            fontWeight: '300'
                        }}
                            onPress={() => this.setState({ loginTypeEmail: !this.state.loginTypeEmail })}
                        >Using OTP</Text>
                        <View style={{ borderBottomColor: COLORS.appColor, borderBottomWidth: .5, marginTop: 7, width: 110 }} />
                    </View>

                </View>

                <View style={{
                    marginTop: 10,
                    marginHorizontal: 30

                }}>
                    <Text style={{ color: COLORS.appColor, textAlign: 'center' }}>{this.state.loginError}</Text>
                    {this.state.loginTypeEmail ?

                        <View style={{ marginTop: 10 }}>
                            <Text style={{ color: '#000', fontWeight: 'bold' }}>Your Email</Text>
                            <TextInput
                                style={{
                                    marginTop: 10,
                                    borderColor: '#000',
                                    borderWidth: 1,
                                    borderRadius: 8,
                                    padding: 5,
                                    paddingLeft: 10

                                }}
                                value={this.state.email}
                                placeholder='Enter your Email / Mobile No'
                                onChangeText={(text) => this.setState({ email: text })}
                            />

                            <Text style={{
                                color: '#000',
                                fontWeight: 'bold',
                                marginTop: 20
                            }}>Password</Text>

                            <View style={{
                                marginTop: 10,
                                borderColor: '#000',
                                borderWidth: 1,
                                borderRadius: 8,
                                justifyContent: 'space-between',
                                height: 40,
                                flexDirection: 'row',
                            }}>
                                <TextInput
                                    style={{
                                        paddingLeft: 10,
                                        alignSelf: 'flex-start'
                                    }}
                                    secureTextEntry={hidePass ? true : false}
                                    placeholder='Enter your Password'
                                    value={this.state.password}
                                    onChangeText={(text) => this.setState({ password: text })}
                                />

                                <Icon
                                    name={hidePass ? 'eye-slash' : 'eye'}
                                    size={18}
                                    color="#d9475c"
                                    onPress={() => this.setState({ hidePass: !hidePass })}
                                    style={{
                                        textAlignVertical: 'center',
                                        marginRight: 13
                                    }}
                                />
                            </View>
                            <Pressable style={{
                                backgroundColor: '#d9475c',
                                justifyContent: 'center',
                                marginTop: 40,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 7,
                                elevation: 5
                            }}
                                onPress={() => this.userLogin()}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'center',
                                        color: '#fff',
                                        fontSize: 18

                                    }}
                                >Login</Text>
                            </Pressable>
                        </View>
                        :
                        <View>


                            <Text style={{ color: '#000', fontWeight: 'bold' }}>Phone Number</Text>
                            <TextInput
                                style={{
                                    marginTop: 10,
                                    borderColor: '#000',
                                    borderWidth: 1,
                                    borderRadius: 8,
                                    padding: 5,
                                    paddingLeft: 10

                                }}
                                value={this.state.phNumber}
                                placeholder='Phone Number'
                                onChangeText={(text) => this.setState({ phNumber: text })}
                            />
                            <Pressable style={{
                                backgroundColor: '#d9475c',
                                justifyContent: 'center',
                                marginTop: 40,
                                paddingTop: 10,
                                paddingBottom: 10,
                                borderRadius: 7,
                                elevation: 5
                            }}
                                onPress={() => this.sendOtp()}
                            >
                                <Text
                                    style={{
                                        alignSelf: 'center',
                                        color: '#fff',
                                        fontSize: 18

                                    }} s
                                >Send OTP</Text>
                            </Pressable>
                        </View>
                    }

                    <Text
                        style={{
                            marginTop: 15,
                            color: '#d9475c',
                            fontWeight: 'bold',
                            alignSelf: 'flex-end'
                        }}
                        onPress={() => this.props.navigation.navigate('MainPage')}
                    >
                        Forgot Password?
                    </Text>

                    <View style={{ flexDirection: 'row', marginTop: 40, justifyContent: 'center' }}>
                        <View style={styles.horizontal} />
                        <Text style={{ color: '#97999d', marginLeft: 3, marginRight: 3 }}>Or</Text>
                        <View style={styles.horizontal} />
                    </View>

                    <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'center' }}>
                        <SocialIcon
                            title='Sign In With Facebook'

                            type='facebook'
                        />

                        <SocialIcon
                            title='Sign In With Facebook'

                            type='google'
                        />
                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'center', marginTop: 60 }}>

                        <Text>New on Saath Phere </Text>

                        <Text style={{
                            color: '#d9475c',
                            fontWeight: 'bold',
                        }}
                            onPress={() => this.props.navigation.navigate('SignUp')}
                        >
                            Register Free
                        </Text>


                    </View>
                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>


            </View>
        );
    };
}

export default Login;

const styles = StyleSheet.create({
    transParentView: {
        flex: 1,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'flex-end',

    },
    horizontal: {
        borderTopColor: '#97999d',
        width: 150,
        borderTopWidth: 1,
        height: 0,
        marginTop: 9
    }

})

