
import React from 'react';

import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import DatePicker from 'react-native-datepicker'

import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'
import { Item } from 'react-native-paper/lib/typescript/components/List/List';

const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;



const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class AstroDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            country: 'Select',
            phnoDrpDwn: '+91',
            hidePass: true,
            groomName: '',
            dob:'',
            top:'',
            mangLik:'',
            city_of_birth:''
        }
    }



    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('astro');

        // if(isComplete==='success'){
        //     this.props.navigation.navigate('FamilyDetails')
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        let rel = await userProfileApi._complete_profile_categories();

        this.setState({ religionList: rel });


        let motherT = await userProfileApi._motherTangues();
        this.setState({ mother_toungeList: motherT });

        let gotr = await userProfileApi._gotras();

        this.setState({ gotrasList: gotr });


        let bl = await userProfileApi._bloodGroups();
        this.setState({ bloodGroupList: bl });





    }

    async sendAstroDetails() {
        // console.log(this.state.mangLik);
        // console.log(this.state.dob);
        // console.log(this.state.top);
        // console.log(this.state.city_of_birth);

        if(
            this.state.mangLik !='' &&
            this.state.dob !='' &&
            this.state.top !='' &&
            this.state.city_of_birth !='' 
        ){
       
        await userProfileApi._astroDetails(this.state.user_id,this.state.mangLik,
            this.state.dob,this.state.top,this.state.city_of_birth);

        this.props.navigation.navigate('FamilyDetails');
        }


    }


    render() {
        const { hidePass } = this.state
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                         }
                        }
                    >Astro Details</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Manglik *</Text>

                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={['Yes','No']}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80,}}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor:'#fff', marginTop: 0,
                                borderColor:COLORS.appColor,
                                borderWidth:1,
                                height:100
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{fontSize:15}}
                        

                            onSelect={(index, val) =>
                                this.setState({ mangLik: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15,
                                
                            }}
                            textStyle={{ marginLeft: 10 ,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


<Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Date Of Birth*</Text>

                        <View style={{
                            marginTop: 7,
                            borderColor: '#000',
                            
                            justifyContent: 'space-between',
                            height: 40,

                            flexDirection: 'row',

                        }}>
                            <DatePicker
                                style={Styles.datepicker}
                                date={this.state.dob}
                                mode="date"
                                placeholder="DOB"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    color: '#fff',
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36,

                                    },

                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(text) => { this.setState({dob:text}) }}
                            />
                        </View>


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Time of Birth *</Text>
                            <TextInput
                            value={this.state.top}
                            onChangeText={(text) => this.setState({ top: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            City of Birth *</Text>
                            <TextInput
                            value={this.state.city_of_birth}
                            onChangeText={(text) => this.setState({ city_of_birth: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        


                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                    // onPress={()=>this.props.navigation.navigate('FamilyDetails')}
                    onPress={()=> this.sendAstroDetails()}
                    
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>                
                </View>
            </ScrollView>
        );
    };
}



export default AstroDetails;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    },
    datepicker: {
        flex: 1,
        height: 40,
        borderColor: '#b84127',
        
        marginTop: 10,
        
        width: width,
        backgroundColor: "#fff",
        // color:'#fff'
    
      },
})