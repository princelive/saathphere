import * as React from 'react';
import {
    DrawerContentScrollView,
    DrawerItem,
    DrawerItemList,
} from '@react-navigation/drawer';
import { Pressable } from 'react-native';
import { View, Text, Button, Dimensions, StyleSheet, Image } from 'react-native'
import { Modal } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Subs from "react-native-vector-icons/AntDesign"
import M_Icon from "react-native-vector-icons/MaterialIcons"
import P from "react-native-vector-icons/Ionicons"
import Ad_Icon from "react-native-vector-icons/Entypo"
import { Avatar, Caption, Drawer, Title } from 'react-native-paper';
import { color } from 'react-native-elements/dist/helpers';
const vw = Dimensions.get('window').width / 100;
const vh = Dimensions.get('window').height / 100;
const hieght = Dimensions.get('screen').height;


const SideMenu = (props) => {

    return (
        <View style={{ flex: 1, backgroundColor: "#f6f7fb" }}>

            <Modal
                animationType="slide"
                transparent={true}
                visible={false}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");

                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Sure to logout?</Text>

                        <View style={{ flexDirection: 'row' }}>
                            <Pressable
                                style={{
                                    backgroundColor: '#250c3b',
                                    width: 100,
                                    padding: 7,
                                    marginTop: 10,
                                    borderRadius: 15,
                                    marginRight: 5
                                }}
                                onPress={() => logout('Yes')}
                            >
                                <Text style={{
                                    color: '#fff',
                                    textAlign: 'center',

                                }}>Yes</Text>
                            </Pressable>

                            <Pressable
                                style={{
                                    backgroundColor: '#250c3b',
                                    width: 100,
                                    padding: 7,
                                    marginTop: 10,
                                    borderRadius: 15,
                                    marginLeft: 5
                                }}
                                onPress={() => logout('No')}
                            >
                                <Text style={{
                                    color: '#fff',
                                    textAlign: 'center',

                                }}>No</Text>
                            </Pressable>
                        </View>
                    </View>
                </View>
            </Modal>
    
            <View style={{ backgroundColor: '#D9475C', height: vh * 15, justifyContent: 'center' }}>

                <View style={{ flexDirection: 'row', marginTop: vh * 2, marginLeft: 20 }}>
                    <Image style={{
                        height: 60, width: 60,
                        borderRadius: 400 / 2,
                        borderWidth: 3, borderColor: '#fff'
                    }} source={require('../images/girl_image.jpg')} />

                    <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                        <Title style={styles.title} >Samira</Title>
                    </View>

                    <Pressable style={{ marginLeft: 'auto', right: 20, justifyContent: 'center' }}
                        onPress={() => props.navigation.navigate('Page1')}
                    >
                        {/* <Subs name="edit" style={{ fontSize: 5 * vw, color: '#fff', }}
                            onPress={() => pickImage()}
                        /> */}

                        <Subs name="edit" style={{ fontSize: 5 * vw, color: '#fff', }}

                        />
                    </Pressable>
                </View>
            </View>
       
            <DrawerContentScrollView {...props}>
                <DrawerItemList {...props} />
                <View style={styles.userSection}>

                    <DrawerItem

                        icon={() => (<P name="person-outline" size={vh * 3} color={'#250c3b'} />)}
                        label='Search'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />
                    <DrawerItem

                        icon={() => (<Subs name="bars" size={vh * 3} color={'#250c3b'} />)}
                        label='Search by Profile ID'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1') 
                        props.navigation.closeDrawer()}}
                    />

                    <DrawerItem

                        icon={() => (<Subs name="bars" size={vh * 3} color={'#250c3b'} />)}
                        label='Saved Search'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1') 
                        props.navigation.closeDrawer()}}
                    />

                    <DrawerItem

                        icon={() => (<M_Icon name="history" size={vh * 3} color={'#250c3b'} />)}
                        label='My Matches'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />

                    <DrawerItem

                        icon={() => (<Icon name="upload" size={vh * 3} color={'#250c3b'} />)}
                        label='My Contacts'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />

                    <DrawerItem

                        icon={() => (<Icon name="upload" size={vh * 3} color={'#250c3b'} />)}
                        label='Shortlisted'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />


                    <DrawerItem

                        icon={() => (<M_Icon name="videocam" size={vh * 3} color={'#250c3b'} />)}
                        label='Phonebook'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1') 
                        props.navigation.closeDrawer()}}
                    />




                    <DrawerItem

                        icon={() => (<M_Icon name="settings" size={vh * 3} color={'#250c3b'} />)}
                        label='Profile Visitors'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />
                    <DrawerItem

                        icon={() => (<Ad_Icon name="modern-mic" size={vh * 3} color={'#250c3b'} />)}
                        label='Setting'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Setting')
                        props.navigation.closeDrawer() }}
                    />


                    <DrawerItem

                        icon={() => (<M_Icon name="settings" size={vh * 3} color={'#250c3b'} />)}
                        label='Help &amp; Support'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1')
                        props.navigation.closeDrawer() }}
                    />
                    <DrawerItem

                        icon={() => (<Ad_Icon name="modern-mic" size={vh * 3} color={'#250c3b'} />)}
                        label='Terms &amp; Conditions'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1') 
                        props.navigation.closeDrawer()}}
                    />

                    <DrawerItem

                        icon={() => (<Ad_Icon name="modern-mic" size={vh * 3} color={'#250c3b'} />)}
                        label='Refer &amp; Earns'
                        labelStyle={styles.label_style}
                        onPress={() => { props.navigation.navigate('Page1') 
                        props.navigation.closeDrawer()}}
                    />

                    <DrawerItem

                        icon={() => (<M_Icon name="logout" size={vh * 3} color={'#250c3b'} />)}
                        label='Logout'
                        labelStyle={styles.label_style}
                        onPress={() => {
                            setModalVisible(true)
                            props.navigation.closeDrawer()
                        }}
                    />

                    <View style={{ flex: 1, height: 1, marginStart: 20, marginEnd: 20, backgroundColor: '#BEBCB0' }}></View>

                    <Text style={{ fontSize: 17, fontWeight: 'bold', marginTop: 25, marginStart: 20 }}>Follow Us</Text>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', margin: 10 }}>
                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C'
                        }} source={require('../images/girl_image.jpg')} />

                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C',
                        }} source={require('../images/girl_image.jpg')} />

                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C',
                        }} source={require('../images/girl_image.jpg')} />

                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C',
                        }} source={require('../images/girl_image.jpg')} />

                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C',
                        }} source={require('../images/girl_image.jpg')} />

                        <Image style={{
                            height: 20, width: 20,
                            borderRadius: 400 / 2,
                            backgroundColor: '#D9475C',
                        }} source={require('../images/girl_image.jpg')} />
                    </View>

                    <View style={styles.button_design}>

                        <Text style={{ fontSize: 11, color: '#FFF', fontFamily: 'bold', justifyContent: 'center', textAlign: 'center', marginTop: 10 }}> Upgrade Membership</Text>
                    </View>

                </View>
            </DrawerContentScrollView>

            {/* </DrawerContentScrollView> */}

        </View>
    )
}

export default SideMenu;

const styles = StyleSheet.create({
    userSection: {

        backgroundColor: '#f6f7fb'
    },
    title: {
        fontWeight: 'normal',
        color: '#fff'
    },
    caption: {
        fontSize: 12
    },
    label_style: {
        fontSize: 15,
        color: '#250c3b',
        fontWeight: 'bold'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    button_design: {
        backgroundColor: '#D9475C',
        height: 40,
        width: 200,
        borderRadius: 5,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 30,
        marginBottom: 20,
        alignSelf: 'center'
    },


})