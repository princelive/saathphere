
import React from 'react';

import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';

import ModalDropdown from 'react-native-modal-dropdown';


import DatePicker from 'react-native-datepicker'
import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'



const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class BasicAndLifeStyle extends Component {

    constructor(props) {
        super(props);
        this.state = {

            country: 'Select',
            phnoDrpDwn: '+91',
            hidePass: true,
            groomName: '',
            ageList: [],
            age: '',
            token: '',
            user_id: '',

            heightList: [],
            height: '',

            weight: '',
            weightList: [],

            bloodGroupList: [],
            bloodGroup: '',

            sunSign: '',
            location: '',
            growUpIn: '',
            helthInformation: '',
            disability: '',
            dob: '',
            maritalStatus: ''



        }
    }



    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('basic');

        // if (isComplete === 'success') {
        //     this.props.navigation.navigate('ReligiousBackground')
        // }
        // console.log('com',isComplete);

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        let age = await userProfileApi._age();
        console.log(age);
        this.setState({ ageList: age });


        let hv = await userProfileApi._groomHeight();
        this.setState({ heightList: hv });

        let wv = await userProfileApi._groomWeights();

        this.setState({ weightList: wv });


        let bl = await userProfileApi._bloodGroups();
        this.setState({ bloodGroupList: bl });





    }

    setMaritalStatus(text) {
        if (text === "Married") {
            this.setState({ maritalStatus: 1 });
        } else {
            this.setState({ maritalStatus: 0 });
        }
    }

    async sendBasicAndLifeStyle() {

        //alert('hi');
        if (
            this.state.age != '' &&
            this.state.dob != '' &&
            this.state.height != '' &&
            this.state.weight != '' &&
            this.state.growUpIn != '' &&
            this.state.sunSign != '' &&
            this.state.bloodGroup != '' &&
            this.state.helthInformation != '' &&
            this.state.disability != ''
        ) {

            await userProfileApi._basicLifeStyle(this.state.user_id, this.state.age, this.state.dob,
                this.state.height, this.state.weight, this.state.growUpIn, this.state.sunSign,
                this.state.bloodGroup,
                this.state.helthInformation, this.state.disability);


            this.props.navigation.navigate('ReligiousBackground');
        }


    }

    render() {
        const { hidePass } = this.state
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Basic And Lifestyle</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Age*</Text>

                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.ageList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ age: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Date Of Birth*</Text>

                        <View style={{
                            marginTop: 7,
                            borderColor: '#000',

                            justifyContent: 'space-between',
                            height: 40,

                            flexDirection: 'row',

                        }}>
                            <DatePicker
                                style={Styles.datepicker}
                                date={this.state.dob}
                                mode="date"
                                placeholder="DOB"
                                format="YYYY-MM-DD"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    color: '#fff',
                                    dateIcon: {
                                        position: 'absolute',
                                        left: 0,
                                        top: 4,
                                        marginLeft: 0
                                    },
                                    dateInput: {
                                        marginLeft: 36,

                                    },

                                    // ... You can check the source to find the other keys.
                                }}
                                onDateChange={(text) => { this.setState({ dob: text }) }}
                            />
                        </View>




                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Height*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.heightList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ height: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Weight*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.weightList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1,
                                height: 200
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ weight: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Grow up in*</Text>


                        <TextInput
                            value={this.state.growUpIn}
                            onChangeText={(text) => this.setState({ growUpIn: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />





                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Sun Sign*</Text>
                        <TextInput
                            value={this.state.sunSign}
                            onChangeText={(text) => this.setState({ sunSign: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Blood Group*</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.bloodGroupList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ bloodGroup: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Health Information*</Text>

                        <TextInput
                            value={this.state.helthInformation}
                            onChangeText={(text) => this.setState({ helthInformation: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Diability*</Text>

                        <TextInput
                            value={this.state.disability}
                            onChangeText={(text) => this.setState({ disability: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />
                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}

                        // onPress={() => this.props.navigation.navigate('ReligiousBackground')}
                        onPress={() => this.sendBasicAndLifeStyle()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>



                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}


const height = Dimensions.get("window").height;
const width = Dimensions.get("window").width;
export default BasicAndLifeStyle;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    },
    datepicker: {
        flex: 1,
        height: 40,
        borderColor: '#b84127',

        marginTop: 10,

        width: width,
        backgroundColor: "#fff",
        // color:'#fff'

    },
})