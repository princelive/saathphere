import * as React from 'react';
import { Text, View, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Dimensions } from 'react-native';
import { Pressable } from 'react-native';
import { COLORS } from '../../styles/COLOR';
import Entypo from 'react-native-vector-icons/Entypo'
import Fontawesom5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicon from 'react-native-vector-icons/Ionicons'
import { Component } from 'react';
import MembershipPlanApi from '../../Api/MembershipPlanApi';


const wv = Dimensions.get('window').width;


export default class Membership extends Component {

    constructor(props) {
        super(props)
        this.state = {
            planData: '',
            basicData: '',
            silverData: '',
            goldData: ''
        }
    }

    async componentDidMount() {

        let data = await MembershipPlanApi.membershipPlan();
        this.setState({ basicData: data.plans[0] })
        this.setState({ silverData: data.plans[1] })
        this.setState({ goldData: data.plans[2] })

    }

    render() {
        return (

            <ScrollView >
                <SafeAreaView style={[styles.page_background, { marginBottom: 40 }]}>

                    {/* ****** header *********** */}

                    <View style={{ flex: 1 }}>
                        <View style={{
                            height: 50,

                            backgroundColor: COLORS.appColor,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',

                        }}>
                            <Ionicon name='ios-arrow-back-outline' color="#fff" size={30}
                                style={{
                                    alignItems: 'center',
                                    marginLeft: 20,
                                    marginRight: 20,

                                }}
                                onPress={() => this.props.navigation.goBack(null)}
                            />
                            <Text style={{ fontSize: 20, color: '#fff' }}>Membership</Text>
                            <Entypo name='dots-three-vertical' color="#fff" size={20}
                                style={{
                                    marginRight: 20
                                }}
                            />
                        </View>

                        {/* ****** header close ***** */}


                        <View style={styles.bigger_card}>
                            <View style={{
                                flexDirection: 'row', marginHorizontal: 20, justifyContent: 'space-between',
                                paddingVertical: 20,

                            }}>

                                <View>
                                    <Text style={{ color: COLORS.appColor, fontWeight: 'bold', fontSize: 18 }}>JS Exclusive</Text>
                                    <Text style={styles.textStyle}>Assisted services +eAdvantage</Text>
                                </View>

                                <Pressable
                                    style={{
                                        backgroundColor: COLORS.appColor,
                                        alignItems: 'center',
                                        paddingLeft: 15,
                                        paddingRight: 15,
                                        flexDirection: 'row',
                                        borderRadius: 5
                                    }}
                                >
                                    <Text style={{ color: '#fff' }}>{'\u20B9'} 13,100</Text>
                                    <Text style={{ color: '#fff', marginLeft: 10, fontSize: 11 }}>20000</Text>
                                </Pressable>

                            </View>

                            <View style={{ borderBottomColor: '#000', borderBottomWidth: .7, marginHorizontal: 20 }} />

                            <Text style={{ color: COLORS.appColor, marginHorizontal: 20, marginTop: 20 }}>A Relationship Manager is assigned to you who</Text>
                            <Text style={[styles.textStyle, { marginLeft: 30, marginTop: 6 }]}>Works on your profile to ensure it gets noticed</Text>
                            <Text style={[styles.textStyle, { marginLeft: 30, marginTop: 6 }]}>Understands qualities that you are booking for in your desired partner.</Text>
                            <Text style={[styles.textStyle, { marginLeft: 30, marginTop: 6 }]}>Sends interested to handpicked profiles matching your criteria.</Text>
                            <Text style={[styles.textStyle, { marginLeft: 30, marginTop: 6 }]}>Contack profiles shortlisted by you and arrange meetings.</Text>

                            <Text style={{ color: COLORS.appColor, marginHorizontal: 20, marginTop: 10 }}>Also get benefits of Js Boost and made your number visible to fee member.</Text>

                        </View>

                        <ScrollView
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}>
                            <View style={{ flexDirection: 'row', flex: 1 }} >

                                <Pressable
                                    onPress={() => this.props.navigation.navigate('Membership_Plan',{plan_details:this.state.basicData})}
                                >
                                    <View style={[styles.listcontainer, { justifyContent: 'space-between', marginTop: 20 }]}>

                                        <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                            <Text style={{
                                                color: COLORS.appColor,
                                                fontSize: 15,
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            }}>{this.state.basicData.plan_name}</Text>

                                            <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: '#000' }]}>
                                                {this.state.basicData.message} </Text>
                                        </View>
                                        <View style={{
                                            backgroundColor: COLORS.appColor,
                                            borderBottomLeftRadius: 5,
                                            borderBottomRightRadius: 5,
                                            padding: 13
                                        }}>
                                            <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} {this.state.basicData.price}</Text>
                                        </View>
                                    </View>
                                </Pressable>

                                <Pressable
                                    onPress={() => this.props.navigation.navigate('Membership_Plan',{plan_details:this.state.silverData})}
                                >
                                    <View style={[styles.listcontainer, { justifyContent: 'space-between', marginTop: 20 }]}>

                                        <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                            <Text style={{
                                                color: COLORS.appColor,
                                                fontSize: 15,
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            }}>{this.state.silverData.plan_name}</Text>

                                            <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: '#000' }]}>
                                                {this.state.silverData.message} </Text>
                                        </View>
                                        <View style={{
                                            backgroundColor: COLORS.appColor,
                                            borderBottomLeftRadius: 5,
                                            borderBottomRightRadius: 5,
                                            padding: 13
                                        }}>
                                            <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} {this.state.silverData.price}</Text>
                                        </View>
                                    </View>
                                </Pressable>

                                <Pressable
                                    onPress={() => this.props.navigation.navigate('Membership_Plan',{plan_details:this.state.goldData})}
                                >
                                    <View style={[styles.listcontainer, { justifyContent: 'space-between', marginTop: 20 }]}>

                                        <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                            <Text style={{
                                                color: COLORS.appColor,
                                                fontSize: 15,
                                                fontWeight: 'bold',
                                                textAlign: 'center'
                                            }}>{this.state.goldData.plan_name}</Text>

                                            <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: '#000' }]}>
                                                {this.state.goldData.message} </Text>
                                        </View>
                                        <View style={{
                                            backgroundColor: COLORS.appColor,
                                            borderBottomLeftRadius: 5,
                                            borderBottomRightRadius: 5,
                                            padding: 13
                                        }}>
                                            <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} {this.state.goldData.price}</Text>
                                        </View>
                                    </View>
                                </Pressable>

                            </View>

                        </ScrollView>
                        <Pressable
                            style={{
                                backgroundColor: COLORS.appColor,
                                marginTop: 30,
                                padding: 10,
                                marginHorizontal: 40,
                                elevation: 10,
                                borderRadius: 20,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                        >
                            <Text style={{ color: '#fff' }}>You need help for buying?</Text>
                        </Pressable>

                    </View>

                </SafeAreaView >
            </ScrollView>
        );
    }
}


const styles = {
    page_background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f1f0f6',

    },

    MainContainer: {
        marginTop: 20
    },

    listcontainer: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        width: 150,
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginBottom: 10,
        paddingBottom: 0

    },

    button_design: {
        backgroundColor: COLORS.appColor,
        borderRadius: 5,
        shadowOpacity: 0.26,
        elevation: 8,

    },

    small_button_design: {
        backgroundColor: '#fff',
        width: 150,
        height: hp('4%'),
        borderRadius: 6,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 20,
        marginBottom: 10,
        alignSelf: 'center'
    },

    bigger_card: {
        backgroundColor: '#FFF',
        flexDirection: 'column',
        flex: 1,
        flex_wrap: 'wrap',
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 10,
        marginBottom: 20,
        paddingBottom: 15,
        paddingTop: 10,
        marginTop: 20
    },

    design_image: {
        height: hp('18%'),
        width: 150,
        resizeMode: 'stretch',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
    },

    subHeading: {
        fontSize: 14,
        color: '#000',
        textAlign: 'left',
        marginTop: 20,
        marginStart: 10,
        fontWeight: 'bold'
    },
    profileDetailsText: {
        fontSize: 10,
        color: '#000',
        textAlign: 'left',
        marginStart: 5,
        marginTop: 5
    },
    circleImgBg: {
        backgroundColor: COLORS.appColor,
        marginEnd: 0,
        marginTop: -30,
        elevation: 10,
        height: 40,
        width: 40,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    circleImgBg_small: {
        backgroundColor: COLORS.appColor,
        marginEnd: 5,
        height: 25,
        width: 25,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 12,
        color: '#5a5b5c'
    }


}
