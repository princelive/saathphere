import * as React from 'react';
import { Text, View, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Dimensions } from 'react-native';
import { Pressable } from 'react-native';
import { COLORS } from '../../styles/COLOR';
import Entypo from 'react-native-vector-icons/Entypo'
import Fontawesom5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicon from 'react-native-vector-icons/Ionicons'
import { Component } from 'react';
import { WebView } from 'react-native-webview';
import RenderHTML from 'react-native-render-html';
const wv = Dimensions.get('window').width;


export default class Membership extends Component {

    constructor(props) {
        super(props);
        this.state = {
            detailsData: ''

        }
    }


    render() {
        const { plan_details } = this.props.route.params
        console.log(plan_details)
        return (

            <ScrollView >
                <SafeAreaView style={[styles.page_background, { marginBottom: 40 }]}>

                    {/* *********** Header start ********* */}
                    <View style={{ flex: 1 }}>
                        <View style={{
                            height: 50,

                            backgroundColor: COLORS.appColor,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',

                        }}>
                            <Ionicon name='ios-arrow-back-outline' color="#fff" size={30}
                                style={{
                                    alignItems: 'center',
                                    marginLeft: 20,
                                    marginRight: 20,

                                }}
                                onPress={() => this.props.navigation.goBack(null)}
                            />
                            <Text style={{ fontSize: 20, color: '#fff' }}>Membership</Text>
                            <Entypo name='dots-three-vertical' color="#fff" size={20}
                                style={{
                                    marginRight: 20
                                }}
                            />
                        </View>


                        {/* Header close */}


                        <View >
                            <View style={{
                                flexDirection: 'row', marginHorizontal: 20, justifyContent: 'space-between',
                                paddingVertical: 20,

                            }}>

                                <View>
                                    <Text style={{ color: COLORS.appColor, fontWeight: 'bold', fontSize: 18 }}>{plan_details.plan_name}</Text>
                                    <Text style={styles.textStyle}>{plan_details.message} {plan_details.duration}</Text>
                                </View>



                            </View>


                            <Text style={{ color: '#000', fontWeight: 'bold', fontSize: 22, marginTop: 20, marginLeft: 20 }}>Basic Benefits</Text>


                            <View style={{ flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginTop: 10 }}>
  
                                <RenderHTML style={styles.textStyle} source={{ html:plan_details.benefits }} />
                           
                            </View>


                            {/* <View style={{ flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginTop: 3 }}>
                                <Entypo name='check' color='#4fba43' size={20} />
                                <Text style={[styles.textStyle, { fontSize: 15, marginLeft: 5 }]}>Message and chat with unlimited users</Text>
                            </View>

                            <View style={{ flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginTop: 3 }}>
                                <Entypo name='check' color='#4fba43' size={20} />
                                <Text style={[styles.textStyle, { fontSize: 15, marginLeft: 5 }]}>Message and chat with unlimited users</Text>
                            </View>

                            <View style={{ flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginTop: 3 }}>
                                <Fontawesom5 name='times' color={COLORS.appColor} size={20} />
                                <Text style={[styles.textStyle, { fontSize: 15, marginLeft: 10 }]}>Message and chat with unlimited users</Text>
                            </View>

                            <View style={{ flexDirection: 'row', marginLeft: 20, alignItems: 'center', marginTop: 3 }}>
                                <Fontawesom5 name='times' color={COLORS.appColor} size={20} />
                                <Text style={[styles.textStyle, { fontSize: 15, marginLeft: 10 }]}>Message and chat with unlimited users</Text>
                            </View> */}



                        </View>



                        <View style={styles.bigger_card}>

                            <View style={{ flexDirection: 'row', marginHorizontal: 9, justifyContent: 'center' }}>

                                <View style={{
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                    borderColor: '#000',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    marginRight: 6
                                }}>

                                    <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                        <Text style={{
                                            // color: COLORS.appColor,
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            textAlign: 'center'
                                        }}>{plan_details.duration}</Text>

                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            {plan_details.message} </Text>
                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            Rs. {plan_details.price} </Text>
                                    </View>
                                    <View style={{
                                        backgroundColor: COLORS.appColor,
                                        borderBottomLeftRadius: 5,
                                        borderBottomRightRadius: 5,
                                        padding: 13
                                    }}>
                                        <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} {plan_details.price}</Text>
                                    </View>
                                </View>

                                {/* <View style={{
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                    borderColor: '#000',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    marginRight: 6
                                }}>

                                    <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                        <Text style={{
                                            // color: COLORS.appColor,
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            textAlign: 'center'
                                        }}>6 Months</Text>

                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            Unlimited Message </Text>
                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            Rs. 5550 </Text>
                                    </View>
                                    <View style={{
                                        backgroundColor: COLORS.appColor,
                                        borderBottomLeftRadius: 5,
                                        borderBottomRightRadius: 5,
                                        padding: 13
                                    }}>
                                        <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} 5000</Text>
                                    </View>
                                </View>

                                <View style={{
                                    justifyContent: 'space-between',
                                    marginTop: 20,
                                    borderColor: '#000',
                                    borderWidth: 1,
                                    borderRadius: 5,
                                    marginRight: 6
                                 }}>

                                    <View style={{ height: hp('15%'), alignItems: 'center', flex: 1, justifyContent: 'center' }}>
                                        <Text style={{
                                            // color: COLORS.appColor,
                                            fontSize: 18,
                                            fontWeight: 'bold',
                                            textAlign: 'center'
                                        }}>Unlimited</Text>

                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            Unlimited Message </Text>
                                        <Text style={[styles.textStyle, { textAlign: 'center', marginTop: 3, fontSize: 10, color: COLORS.appColor }]}>
                                            Rs. 5550 </Text>
                                    </View>
                                    <View style={{
                                        backgroundColor: COLORS.appColor,
                                        borderBottomLeftRadius: 5,
                                        borderBottomRightRadius: 5,
                                        padding: 13
                                    }}>
                                        <Text style={{ textAlign: 'center', color: '#fff' }}>From {'\u20B9'} 5000</Text>
                                    </View>
                                </View> */}

                            </View>

                        </View>




                        <Pressable
                            style={{
                                backgroundColor: COLORS.appColor,
                                marginTop: 30,
                                padding: 10,
                                marginHorizontal: 40,
                                elevation: 10,
                                borderRadius: 20,
                                justifyContent: 'center',
                                alignItems: 'center'

                            }}
                        >
                            <Text style={{ color: '#fff' }}>You need help for buying?</Text>
                        </Pressable>

                    </View>

                </SafeAreaView >
            </ScrollView>
        );
    }
}


const styles = {
    page_background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f1f0f6',

    },

    MainContainer: {
        marginTop: 20
    },

    listcontainer: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        width: 150,
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginBottom: 10,
        paddingBottom: 0

    },

    button_design: {
        backgroundColor: COLORS.appColor,
        borderRadius: 5,
        shadowOpacity: 0.26,
        elevation: 8,

    },

    small_button_design: {
        backgroundColor: '#fff',
        width: 150,
        height: hp('4%'),
        borderRadius: 6,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 20,
        marginBottom: 10,
        alignSelf: 'center'
    },

    bigger_card: {
        backgroundColor: '#FFF',
        flexDirection: 'column',
        flex: 1,
        flex_wrap: 'wrap',
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 10,
        marginBottom: 20,
        paddingBottom: 15,
        paddingTop: 10,
        marginTop: 20
    },

    design_image: {
        height: hp('18%'),
        width: 150,
        resizeMode: 'stretch',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
    },

    subHeading: {
        fontSize: 14,
        color: '#000',
        textAlign: 'left',
        marginTop: 20,
        marginStart: 10,
        fontWeight: 'bold'
    },
    profileDetailsText: {
        fontSize: 10,
        color: '#000',
        textAlign: 'left',
        marginStart: 5,
        marginTop: 5
    },
    circleImgBg: {
        backgroundColor: COLORS.appColor,
        marginEnd: 0,
        marginTop: -30,
        elevation: 10,
        height: 40,
        width: 40,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    circleImgBg_small: {
        backgroundColor: COLORS.appColor,
        marginEnd: 5,
        height: 25,
        width: 25,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 12,
        color: '#5a5b5c'
    }


}
