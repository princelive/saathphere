
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
    Image
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import * as ImagePicker from "react-native-image-picker"
import SecureStorage from 'react-native-secure-storage'
import axios from 'axios';





const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class UserUploadPhoto extends Component {

    constructor(props) {
        super(props);
        this.state = {
            imgUri: '',
            token: ''
        }
    }

    async componentDidMount() {
        let t = await SecureStorage.getItem('token');
        this.setState({ token: t });
        // alert(t)
    }


    launchImageLibrary = async () => {
        //this.props.navigation.navigation("ImagePic")
        let options = {
            storageOptions: {
                skipBackup: true,
                path: 'images',

            },
        };
        await ImagePicker.launchImageLibrary(options, (response) => {
            // console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
                alert(response.customButton);
            } else {
                //const source = { uri: response.assets[0].uri };
                console.log("uri " + response.assets[0].uri);

                this.setState({imgUri:response.assets[0].uri})

                this.uploadImg(this.state.token, this.state.imgUri)
            }
        });

    }

    uploadImg = async (token, ur) => new Promise((resolve, reject) => {


        var formData = new FormData();
        //data.append('email', this.state.email);
        // this.setState({})
        formData.append('image',
            {
                uri: ur,
                name: 'userProfile.jpg',
                type: 'image/jpg',


            });


        //   return axios.post('http://metromonial.itcomit.com/api/upload-profile-picture', data).then(response => {
        //       resolve(response)
        //       console.log("uploadres "+response.data);
        //       //setUserName('Krishna');


        //   })
        //       .catch(error => { reject(error) });


        axios({
            url: "http://metromonial.itcomit.com/api/upload-profile-picture",
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })


    });


    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Now Upload Photo</Text>

                    <Image
                        source={{ uri: this.state.imgUri }}
                        style={{
                            height: 90,
                            width: 90,
                            borderRadius: 400 / 2,
                            justifyContent: 'center',
                            alignSelf: 'center',
                            borderColor: '#252041',
                            borderWidth: 3

                        }}
                    />

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text style={{
                            justifyContent: 'center',
                            marginTop: 40, fontSize: 12
                        }}>. Profile with photo gives 10 times better response </Text>
                        <Text style={{
                            justifyContent: 'center',
                            marginTop: 10, fontSize: 12
                        }}>. Add Quality photos, your photos are safe with us </Text>

                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('PartnerBasicInfo')}
                        onPress={() => this.launchImageLibrary()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15
                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default UserUploadPhoto;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})