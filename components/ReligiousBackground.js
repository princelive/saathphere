
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import DatePicker from 'react-native-datepicker'
import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'







const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class ReligiousBackground extends Component {

    constructor(props) {
        super(props);
        this.state = {
            country: 'Select',
            phnoDrpDwn: '+91',
            hidePass: true,
            religionList: [],
            religion: '',
            community: '',
            sub_community: '',
            gotro: '',
            mother_toungeList: [],
            motherTounge: '',
            can_speek: '',
            gotrasList: [],
            gotras: '',

            token: '',
            user_id: '',


        }
    }

    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('religion');

        // if (isComplete === 'success') {
        //     this.props.navigation.navigate('AstroDetails')
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        let rel = await userProfileApi._complete_profile_categories();

        this.setState({ religionList: rel });


        let motherT = await userProfileApi._motherTangues();
        this.setState({ mother_toungeList: motherT });

        let gotr = await userProfileApi._gotras();

        this.setState({ gotrasList: gotr });


        let bl = await userProfileApi._bloodGroups();
        this.setState({ bloodGroupList: bl });





    }

    async sendReligiousBackground() {
        // console.log(this.state.religion);
        // console.log(this.state.community);
        // console.log(this.state.sub_community);
        // console.log(this.state.gotras);
        // console.log(this.state.motherTounge);
        // console.log(this.state.can_speek);

        if (
            this.state.religionList != '' &&
            this.state.community != '' &&
            this.state.sub_community != '' &&
            this.state.gotras != '' &&
            this.state.motherTounge != '' &&
            this.state.can_speek != ''
        ) {

            //alert('hiii');

            await userProfileApi._religion(this.state.user_id, this.state.religion, this.state.community,
                this.state.sub_community, this.state.gotras, this.state.motherTounge, this.state.can_speek,
            );

            this.props.navigation.navigate('AstroDetails');
        }


    }

    render() {
        const { hidePass } = this.state
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Religious Background</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Religion *</Text>

                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.religionList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ religion: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Community *</Text>
                        <TextInput
                            value={this.state.community}
                            onChangeText={(text) => this.setState({ community: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Sub Community *</Text>
                        <TextInput
                            value={this.state.sub_community}
                            onChangeText={(text) => this.setState({ sub_community: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Gothro *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.gotrasList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ gotras: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />









                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Mother Tounge *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.mother_toungeList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 80, }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#f1f1f1', marginTop: 0,
                                borderColor: COLORS.appColor,
                                borderWidth: 1
                                // marginLeft: -100
                            }}
                            defaultTextStyle={{ fontSize: 15 }}


                            onSelect={(index, val) =>
                                this.setState({ motherTounge: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize: 15,

                            }}
                            textStyle={{ marginLeft: 10, fontSize: 15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Can Speek *</Text>
                        <TextInput
                            value={this.state.can_speek}
                            onChangeText={(text) => this.setState({ can_speek: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />


                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('AstroDetails')}
                        onPress={() => this.sendReligiousBackground()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15

                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default ReligiousBackground;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})