import * as React from 'react'
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    Text,
    View,
    useColorScheme,
    ImageBackground,
    Image
} from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen'
import SplashScreen from 'react-native-splash-screen'
import { color } from 'react-native-reanimated';


const image = require('../images/splash_background.jpg');


function WelcomeScreen(props) {

    
    React.useEffect(() => {
        setTimeout(() => {
          
            SplashScreen.hide();
            props.navigation.navigate('AfterSplash');
            //props.navigation.navigate('Video_Player');
            
        }, 3000);
       
      }, [])

    
    return (
      
        <View style={{flex:1}}>
             <StatusBar  barStyle="light-content" backgroundColor="#252041"/>
            {/* <ImageBackground source={image} resizeMode="cover"
             style={{flex:1,justifyContent:'center'}}>
             
              
            </ImageBackground> */}
        </View>

    );
};

export default WelcomeScreen
