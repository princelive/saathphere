
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign';
import React, { Component } from 'react';
import { COLORS } from '../../styles/COLOR';
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import DatePicker from 'react-native-datepicker'
import userProfileApi from '../../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage';
import Spinner from 'react-native-loading-spinner-overlay';


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;

class PartnerBasicInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {

            ageList: [],
            age: '',
            token: '',
            user_id: '',

            heightList: [],
            height: '',

            religionList:[],
            religion:'',

            motherToungList:[],
            motherTounge:'',

            maritalStatus:'',

            spinner:false

        }
    }


    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('partner-basic');

        // if(isComplete==='success'){
        //     this.props.navigation.navigate('PartnerLocationInfo')
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        let age = await userProfileApi._age();
        console.log(age);
        this.setState({ ageList: age });


        let hv = await userProfileApi._groomHeight();
        this.setState({ heightList: hv });

        let wv = await userProfileApi._motherTangues();

        this.setState({ motherToungList: wv });


        let bl = await userProfileApi._complete_profile_categories();
        this.setState({ religionList: bl });





    }

    setMaritalStatus(text){
        if(text==="Married"){
            this.setState({maritalStatus:1});
        }else{
            this.setState({maritalStatus:0});
        }
    }


    async onSubmit(){
        this.setState({spinner:true});
        await userProfileApi._partnerBasicInfo(this.state.user_id,this.state.age,this.state.height,this.state.religion,this.state.motherTounge,this.state.maritalStatus)
        this.setState({spinner:false});
        this.props.navigation.navigate('PartnerLocationInfo');

    }

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />



                    <Spinner
                        visible={this.state.spinner}
                        cancelable={true}


                    />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >Partner Preference</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Basic Info</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Age Range *</Text>

                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.ageList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setState({ age: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10 ,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Height Range *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.heightList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setState({ height: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10 ,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Religion/Community *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.religionList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setState({ religion: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10 ,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Mother Tounge *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={this.state.motherToungList}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor,
                                fontSize:18

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setState({ motherTounge: val })
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10,fontSize:15}}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />






                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Maratial Status *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                               'Married', 'Unmarried'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,
                                borderColor:COLORS.appColor,
                                height:100
                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setMaritalStatus(val)
                            }
                            dropdownTextStyle={{
                                color: '#000',
                                fontSize:15
                            }}
                            textStyle={{ marginLeft: 10,fontSize:15 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('PartnerLocationInfo')}
                        onPress={()=> this.onSubmit()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15
                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default PartnerBasicInfo;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})