
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import userProfileApi from '../../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'
import Spinner from 'react-native-loading-spinner-overlay';



const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class PartnerOtherDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
            createdBy:'',
            diet:''

        }
    }

    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('education');

        // if(isComplete==='success'){
        //     this.props.navigation.navigate('Home')
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });
      

    }

    async onSubmit(){
        //alert(this.state.createdBy+"  "+this.state.diet)

        await userProfileApi._other_details(this.state.user_id,this.state.createdBy,this.state.diet);
    }
    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >Partner Preference</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Other Details</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Profile Created by *</Text>

                        <TextInput
                            value={this.state.createdBy}
                            onChangeText={(text) => this.setState({ createdBy: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Enter Your Name'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Diet *</Text>
                        <ModalDropdown

                            animated={true}
                            isFullWidth={true}
                            options={[
                                'Veg', 'Noveg'
                            ]}
                            defaultValue={'Select Or Type'}

                            containerStyle={{ height: 40, marginTop: 8 }}
                            style={Styles.dropDownStyles}

                            dropdownStyle={{
                                backgroundColor: '#fff', marginTop: 10,

                                // marginLeft: -100
                            }}


                            onSelect={(index, val) =>
                                this.setState({ diet: val })
                            }
                            dropdownTextStyle={{
                                color: '#000'
                            }}
                            textStyle={{ marginLeft: 10 }}
                            renderRightComponent={() => (
                                <AntDesign
                                    name={'down'}
                                    size={18}
                                    color="#000"
                                    style={{ marginLeft: 'auto', marginRight: 10 }}
                                />
                            )}

                        />

                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('')}
                        onPress={()=> this.onSubmit()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15
                            }}
                        >Complete</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default PartnerOtherDetails;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})