import * as React from 'react';
import { Text, View, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Dimensions } from 'react-native';
import { Pressable } from 'react-native';
import { COLORS } from '../styles/COLOR';
import Fontawesom from 'react-native-vector-icons/FontAwesome'
import SideMenu from './SideMenu'
import M_Icon from "react-native-vector-icons/MaterialIcons"
import { Component } from 'react';
import Home_Api from '../Api/Home_Api'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Login from './Login'
import Search from './Search';
import { TouchableOpacity } from 'react-native';


const vh = Dimensions.get('window').height / 100;
const wv = Dimensions.get('window').width;

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
    },
];
class Home extends Component {


    constructor(props) {
        super(props)
        this.state = {
            brideName: '',
            brideAge: '',
            brideLocation: '',
            brideIncom: '',
            maritalStatus: '',
            brideAddress: '',
            brideImage: '',
            location: '',
            just_joined: ''
                    }
    }

    async componentDidMount() {
       
        let res = await Home_Api.getUserDetails();
       
        let recomended = await Home_Api.recomendedCategory();

        // console.log("recomende ",recomended.just_joined[0].user_id);
        this.setState({ just_joined: recomended.just_joined })

        // console.log("just",this.state.just_joined[0].gothro);
        

        this.setState({ brideName: res.personal.bridge_name })
        this.setState({ brideImage: res.basic.photo })
        this.setState({ location: res.location.location })
    }

    render() {
        return (

            <ScrollView >
                <SafeAreaView style={[styles.page_background, { marginBottom: 40 }]}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontSize: 14, color: '#000', textAlign: 'left', marginTop: 20, marginStart: 10 }}>Daily Recommendations</Text>
                        <Text style={{ fontSize: 14, color: '#D94767', textAlign: 'right', marginEnd: 10, marginTop: 20 }}>View More</Text>
                    </View>

                    <View style={styles.bigger_card}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={{ uri: 'http://metromonial.itcomit.com/profile_images/' + this.state.brideImage }}
                                style={{
                                    height: 80, width: 80,
                                    marginTop: 10,
                                    marginStart: 10,
                                    borderRadius: 400 / 2,
                                    borderColor: COLORS.appColor,
                                    borderWidth: 2
                                }}></Image>


                            <View style={{ flexDirection: 'column', marginTop: 20, marginStart: 15 }}>
                                <Text style={{ fontSize: 17, color: '#000', textAlign: 'left', fontWeight: 'bold' }}>{this.state.brideName}</Text>
                                <Text style={{ fontSize: 13, color: '#000', textAlign: 'left', marginTop: 8 }}>Age 23 Year | Location: {this.state.location}</Text>
                                <Text style={{ fontSize: 13, color: '#000', textAlign: 'left', marginTop: 5 }}>Trade School{"\n"} Medical/ Healthcare Professional{"\n"} Rs.0 - 1 Lakhs</Text>
                                <Text style={{ fontSize: 13, color: '#000', textAlign: 'left', marginTop: 5 }}>Never Married</Text>
                            </View>


                        </View>
                        <View style={{ justifyContent: 'space-around', flexDirection: 'row', marginBottom: 10, paddingTop: 12 }}>
                            <View style={{ flexDirection: 'row', display: 'flex', alignItems: 'center' }}>
                                <Pressable style={[styles.circleImgBg_small, { alignItems: 'center' }]}>
                                    <MaterialIcons name='person-search' size={12} color='#fff' />
                                </Pressable>
                                <Text style={{ fontSize: 13, color: '#D94767', fontWeight: '800', alignItems: 'center' }}>Shortlist</Text>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Pressable style={[styles.circleImgBg_small, { alignItems: 'center' }]}>
                                    <Fontawesom name='send' size={9} color='#fff' />
                                </Pressable>

                                <Text style={{ fontSize: 13, color: '#D94767', fontWeight: '900' }}>Send Interest</Text>
                            </View>

                            <Pressable style={{
                                backgroundColor: COLORS.appColor, paddingLeft: 15,
                                paddingRight: 15, paddingTop: 6, paddingBottom: 6, borderRadius: 7,
                                elevation: 8

                            }}>
                                <Text style={{ color: '#fff' }}>Veiw Profile</Text>
                            </Pressable>
                        </View>

                    </View>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={styles.subHeading}>Just Joined</Text>
                        <Text style={{ fontSize: 14, color: '#D94767', textAlign: 'right', marginEnd: 10, marginTop: 20 }}>View More</Text>
                    </View>
                    <Pressable
                        onPress={() => this.props.navigation.navigate('ViewProfileDetails')}>
                        <View style={[styles.MainContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                            <FlatList
                                data={this.state.just_joined}
                                renderItem={({ item, index }) => (
                                    <View style={styles.listcontainer}>
                                        <Image source={require('../images/girl_image.jpg')} style={styles.design_image}></Image>

                                        <Pressable
                                            style={styles.circleImgBg}
                                            onPress={() => alert("hiii")}
                                        >

                                            <Fontawesom name='send' color='#fff'
                                                size={13}
                                                style={{ alignSelf: 'center' }} />
                                        </Pressable>
                                      
                                        <Text style={styles.profileDetailsText}> Age:{item.user_age}</Text>
                                        <Text style={styles.profileDetailsText}> Height:123</Text>
                                        <Text style={styles.profileDetailsText}> Name: {item.user_name}</Text>
                                        <Text style={styles.profileDetailsText}> No Income, Not Working</Text>
                                    </View>
                                )}
                                horizontal={true}
                                scrollEnabled={true}
                                showsHorizontalScrollIndicator={false}
                            // keyExtractor={item => item.id}
                            />

                        </View>
                    </Pressable>


                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <Text style={{ fontSize: 14, color: '#000', textAlign: 'left', marginTop: 10, marginStart: 10 }}>Mutual Matches</Text>
                        <Text style={{ fontSize: 14, color: '#D94767', textAlign: 'right', marginEnd: 10, marginTop: 10 }}>View More</Text>
                    </View>

                    <Pressable
                        onPress={() => this.props.navigation.navigate('ViewProfileDetails')}>
                        <View style={[styles.MainContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                            <FlatList

                                data={DATA}
                                renderItem={({ item, index }) => (
                                    <View style={styles.listcontainer}>
                                        <Image source={require('../images/girl_image.jpg')} style={styles.design_image}></Image>

                                        <Pressable
                                            style={styles.circleImgBg}
                                            onPress={() => alert("hiii")}
                                        >

                                            <Fontawesom name='send' color='#fff'
                                                size={13}
                                                style={{ alignSelf: 'center' }} />
                                        </Pressable>

                                        <Text style={styles.profileDetailsText}> Age:{index}</Text>
                                        <Text style={styles.profileDetailsText}> Height:123</Text>
                                        <Text style={styles.profileDetailsText}> Name:Rani Kumari</Text>
                                        <Text style={styles.profileDetailsText}> No Income, Not Working</Text>
                                    </View>
                                )}
                                horizontal={true}
                                scrollEnabled={true}
                                showsHorizontalScrollIndicator={false}
                            // keyExtractor={item => item.id}
                            />

                        </View>
                    </Pressable>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <Text style={{ fontSize: 14, color: '#000', textAlign: 'left', marginTop: 10, marginStart: 10 }}>Nearby Matches</Text>
                        <Text style={{ fontSize: 14, color: '#D94767', textAlign: 'right', marginEnd: 10, marginTop: 10 }}>View More</Text>
                    </View>

                    <Pressable
                        onPress={() => this.props.navigation.navigate('ViewProfileDetails')}>
                        <View style={[styles.MainContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                            <FlatList
                                data={DATA}
                                renderItem={({ item }) => (
                                    <View style={styles.listcontainer}>
                                        <Image source={require('../images/girl_image.jpg')} style={styles.design_image}></Image>


                                        <Pressable
                                            style={styles.circleImgBg}
                                            onPress={() => alert("hiii")}
                                        >

                                            <Fontawesom name='send' color='#fff'
                                                size={13}
                                                style={{ alignSelf: 'center' }} />
                                        </Pressable>

                                        <Text style={styles.profileDetailsText}> Age:23</Text>
                                        <Text style={styles.profileDetailsText}> Height:123</Text>
                                        <Text style={styles.profileDetailsText}> Name:Rani Kumari</Text>
                                        <Text style={styles.profileDetailsText}> No Income, Not Working</Text>
                                    </View>
                                )}
                                horizontal={true}
                                scrollEnabled={true}
                                showsHorizontalScrollIndicator={false}
                            // keyExtractor={item => item.id}
                            />

                        </View>
                    </Pressable>

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                        <Text style={{ fontSize: 14, color: '#000', textAlign: 'left', marginTop: 10, marginStart: 10 }}>Verified Matches</Text>
                        <Text style={{ fontSize: 14, color: '#D94767', textAlign: 'right', marginEnd: 10, marginTop: 10 }}>View More</Text>
                    </View>

                    <Pressable
                        onPress={() => this.props.navigation.navigate('ViewProfileDetails')}>

                        <View style={[styles.MainContainer, { flexDirection: 'row', justifyContent: 'space-between' }]}>
                            <FlatList

                                data={DATA}
                                renderItem={({ item }) => (
                                    <View style={styles.listcontainer}>
                                        <Image source={require('../images/girl_image.jpg')} style={styles.design_image}></Image>
                                        <Pressable
                                            style={styles.circleImgBg}
                                            onPress={() => alert("hiii")}
                                        >

                                            <Fontawesom name='send' color='#fff'
                                                size={13}
                                                style={{ alignSelf: 'center' }} />
                                        </Pressable>
                                        <Text style={styles.profileDetailsText}> Age:23</Text>
                                        <Text style={styles.profileDetailsText}> Height:123</Text>
                                        <Text style={styles.profileDetailsText}> Name:Rani Kumari</Text>
                                        <Text style={styles.profileDetailsText}> No Income, Not Working</Text>
                                    </View>
                                )}
                                horizontal={true}
                                scrollEnabled={true}
                                showsHorizontalScrollIndicator={false}
                            // keyExtractor={item => item.id}
                            />

                        </View>

                    </Pressable>
                    <Pressable style={{
                        backgroundColor: COLORS.appColor, marginTop: 20,
                        marginBottom: 30,
                        justifyContent: 'center',
                        marginHorizontal: 40,
                        borderRadius: 10,
                        elevation: 10
                    }}
                        onPress={() => this.props.navigation.navigate('Membership')}
                    >
                        <Text style={{
                            textAlign: 'center',
                            color: '#fff',
                            paddingTop: 12,
                            paddingBottom: 12,
                        }}> Upgrade Membership</Text>

                    </Pressable>

                </SafeAreaView >
            </ScrollView>
        );
    }
}
function Search11({navigation}) {
  
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            {/* <Text>Search!</Text> */}
            
        </View>
    );
}

function Chat() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Chat!</Text>
        </View>
    );
}

function Notifications() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Notifications!</Text>
        </View>
    );
}

function Profile() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Profile!</Text>
        </View>
    );
}
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

function MyDrawer() {
    return (

        <Drawer.Navigator initialRouteName="Home" drawerContent={props => <SideMenu{...props} />}>

            <Drawer.Screen name="Home" component={MyTabs}
                options={{
                    headerStyle: { backgroundColor: COLORS.appColor, color: '#fff' },
                    headerTitle: "Recomended",
                    headerTitleStyle: { color: '#fff', textAlign: 'center' },
                    headerTintColor: '#fff',
                    drawerIcon: () => (<M_Icon name="videocam" size={vh * 3} color={'#ffff'} />),


                    headerRight: () => (<M_Icon name="videocam" size={vh * 3} color={'#ffff'} />

                    ),




                }}
            />

        </Drawer.Navigator>
    );
}

function MyTabs() {
    return (
        <Tab.Navigator
            initialRouteName="Home"
            screenOptions={{
                tabBarActiveTintColor: '#e91e63',
                tabBarStyle: { position: 'absolute' },
                tabBarInactiveBackgroundColor: '#d9475c',
                tabBarActiveBackgroundColor: '#d9475c',
                tabBarShowLabel: false,
                headerTintColor: '#FFF',
                headerShown: false,
                headerStyle: {
                    backgroundColor: 'red'
                }
            }}
        >
            <Tab.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <AntDesign
                            name={'home'}
                            size={28}
                            color="#FFF"
                            style={{ marginRight: 10 }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Search"
                component={Search}
                 options={{
                 
                //  tabBarButton: (props) => (<Pressable  {...props} onPress={() => alert(123)} />),
                    tabBarIcon: ({ color, size }) => (
                        <AntDesign
                            name={'search1'}
                            size={20}
                            color="#FFF"
                            style={{ marginRight: 10 }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Chat"
                component={''}
                options={{

                    tabBarIcon: ({ color, size }) => (
                        <AntDesign
                            name={'wechat'}
                            size={18}
                            color="#FFF"
                            style={{ marginRight: 10 }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Notifications"
                component={Notifications}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <AntDesign
                            name={'bells'}
                            size={18}
                            color="#FFF"
                            style={{ marginRight: 10 }}
                        />
                    ),
                }}
            />
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{
                    tabBarIcon: ({ color, size }) => (
                        <AntDesign
                            name={'user'}
                            size={18}
                            color="#FFF"
                            style={{ marginRight: 10 }}
                        />
                        // <MaterialCommunityIcons name="users" color={'#FF0000'} size={18} />
                    ),
                }}
            />
        </Tab.Navigator>
    );
}

export default function App() {
    return (
          
  <MyDrawer /> 
    
 

    );
}

const styles = {
    page_background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f1f0f6',

    },

    MainContainer: {
        marginTop: 20
    },

    listcontainer: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        width: 150,
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginBottom: 10,
        paddingBottom: 20

    },

    button_design: {
        backgroundColor: '#fff',
        width: 300,
        height: hp('7%'),
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 30,
        marginBottom: 20,
        alignSelf: 'center'
    },

    small_button_design: {
        backgroundColor: '#fff',
        width: 150,
        height: hp('4%'),
        borderRadius: 6,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 20,
        marginBottom: 10,
        alignSelf: 'center'
    },

    bigger_card: {
        backgroundColor: '#FFF',
        flexDirection: 'column',
        flex: 1,
        flex_wrap: 'wrap',
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 10,
        marginBottom: 20,
        paddingBottom: 15,
        paddingTop: 10,
        marginTop: 20
    },

    design_image: {
        height: hp('18%'),
        width: 150,
        resizeMode: 'stretch',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
    },

    subHeading: {
        fontSize: 14,
        color: '#000',
        textAlign: 'left',
        marginTop: 20,
        marginStart: 10,
        fontWeight: 'bold'


    },
    profileDetailsText: {
        fontSize: 10,
        color: '#000',
        textAlign: 'left',
        marginStart: 5,
        marginTop: 5
    },
    circleImgBg: {
        backgroundColor: COLORS.appColor,
        marginEnd: 10,
        marginTop: -10,
        alignSelf: 'flex-end',
        height: 30,
        width: 30,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },

    circleImgBg_small: {
        backgroundColor: COLORS.appColor,
        marginEnd: 5,
        height: 25,
        width: 25,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    }


}
