
import {
    Pressable,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
} from 'react-native';


import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import homeApi from '../Api/Home_Api';

const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class Delete extends Component {

    constructor(props) {
        super(props);
        this.state = {
           clickYes:false,
           comment:''

        }
    }

      async onSubmit() {
              await homeApi.deleteProfile(this.state.comment);
     }

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',

                           }
                            }
                        >Delete Profile</Text>
                    </View>


                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>

                        <Text style={{ fontSize: 20, color: '#000' }}> Why You Want to Delete Your Profile</Text>

                        <TextInput
                            value={this.state.createdBy}
                            onChangeText={(text) => this.setState({ createdBy: text })}
                            style={{
                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 10,
                                height: 200,
                                borderRadius: 5,
                                paddingLeft: 15,
                                    
                            }}
                            onChangeText={(text) => this.setState({ comment: text })}
                            placeholder='Write Here.........'
                        />

                        <Pressable style={{
                            backgroundColor: '#d9475c',
                            justifyContent: 'center',
                            marginTop: 40,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 7,
                            elevation: 5,
                            marginHorizontal: 40,
                            marginBottom: 20
                        }}
                            // onPress={() => this.props.navigation.navigate('')}
                            onPress={() => this.onSubmit()}
                        >
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 15
                                }}
                            >Delete</Text>
                        </Pressable>

                    </View>


                </View>
            </ScrollView>
        );
    };
}



export default Delete;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})