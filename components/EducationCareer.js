
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import DropDownPicker from "react-native-custom-dropdown";
import Icon from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome5';
import AntDesign from 'react-native-vector-icons/AntDesign'
import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { TextInput } from 'react-native';
import { Dimensions } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';

import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'
import Spinner from 'react-native-loading-spinner-overlay';


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class EducationCareer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            heghest_qualification: '',
            collage_attended: '',
            annual_income: '',
            working_with: '',
            working_as: '',
            employer_name: '',

            token:'',
            user_id:'',
            spinner:false

        }
    }

    async componentDidMount() {

        // let isComplete = await userProfileApi._profileStatus('education');

        // if(isComplete==='success'){
        //     this.props.navigation.navigate('LocationGroom');
        // }

        let tokn = await SecureStorage.getItem('token');
        let userId = await SecureStorage.getItem('user_id');
        this.setState({ user_id: userId });
        this.setState({ token: tokn });

        

    }

    async sendEducaAndCareer(){

        // this.setState({spinner:true});
        // if(this.state.heghest_qualification!='' && this.state.collage_attended!=''
        //             ,this.state.annual_income!='' && this.state.working_with!='',
        //             this.state.working_as!='' && this.state.employer_name!=''){
                    
                    
                    await userProfileApi._education_career(this.state.user_id,this.state.heghest_qualification,
                        this.state.collage_attended,this.state.annual_income,this.state.working_with,this.state.working_as,
                        this.state.employer_name);
                        
                       

        // }
        // this.setState({spinner:false});

        this.props.navigation.navigate('LocationGroom');

    }



    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fff' }}>
                <View style={{ flex: 1, backgroundColor: '#fafafa' }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    {/* <Spinner
                        visible={this.state.spinner}
                        cancelable={true}


                    /> */}

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>

                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',


                            }
                            }
                        >User Profile Details</Text>
                    </View>


                    <Text
                        style={{
                            color: COLORS.appColor,
                            alignSelf: 'center',
                            alignItems: 'center',
                            fontSize: 20,
                            marginTop: 30


                        }
                        }
                    >Education {'&'} Career</Text>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>
                        <Text>Highest Qualification *</Text>

                        <TextInput
                            value={this.state.heghest_qualification}
                            onChangeText={(text) => this.setState({ heghest_qualification: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Type Here'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            College Attended *</Text>
                        <TextInput
                            value={this.state.collage_attended}
                            onChangeText={(text) => this.setState({ collage_attended: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Type Here'
                        />


                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Annual Income *</Text>
                        <TextInput
                            value={this.state.annual_income}
                            onChangeText={(text) => this.setState({ annual_income: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Type Here'
                        />



                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Working With *</Text>
                        <TextInput
                            style={{
                                marginTop: 10,
                                borderColor: '#000',
                                borderWidth: 1,
                                borderRadius: 8,
                                padding: 5,
                                paddingLeft: 10

                            }}

                            placeholder='Type Here'
                            value={this.state.working_with}
                            onChangeText={text => this.setState({ working_with: text })}
                        />






                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Working as *</Text>
                            <TextInput
                            value={this.state.working_as}
                            onChangeText={(text) => this.setState({ working_as: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Type Here'
                        />

                        <Text style={{ color: '#000', marginTop: 25, marginHorizontal: 0 }}>
                            Employer Name *</Text>
                            <TextInput
                            value={this.state.employer_name}
                            onChangeText={(text) => this.setState({ employer_name: text })}
                            style={{
                                borderColor: '#000',

                                borderColor: '#000',
                                borderWidth: 1,
                                marginTop: 8,
                                height: 40,
                                borderRadius: 5,
                                paddingLeft: 15

                            }}
                            placeholder='Type Here'
                        />


                        {/* ***** */}

                    </View>



                    <Pressable style={{
                        backgroundColor: '#d9475c',
                        justifyContent: 'center',
                        marginTop: 40,
                        paddingTop: 10,
                        paddingBottom: 10,
                        borderRadius: 7,
                        elevation: 5,
                        marginHorizontal: 40,
                        marginBottom: 20
                    }}
                        // onPress={() => this.props.navigation.navigate('LocationGroom')}
                        onPress={()=> this.sendEducaAndCareer()}
                    >
                        <Text
                            style={{
                                alignSelf: 'center',
                                color: '#fff',
                                fontSize: 15
                            }}
                        >Next</Text>
                    </Pressable>


                    <View
                        style={{

                            borderTopColor: '#d9475c',
                            width: 150,
                            borderTopWidth: 1.5,
                            height: 0,
                            marginTop: 9,
                            alignSelf: 'center'
                        }}
                    />
                </View>
            </ScrollView>
        );
    };
}



export default EducationCareer;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})