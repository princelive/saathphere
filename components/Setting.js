
import {
    Pressable,
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    useColorScheme,
    View,
} from 'react-native';

import React, { Component } from 'react';
import { COLORS } from '../styles/COLOR'
import { Dimensions } from 'react-native';

import { Overlay } from 'react-native-elements';
import userProfileApi from '../Api/UserProfileApi';
import SecureStorage from 'react-native-secure-storage'


const globalStyle = require('../styles');
const vw = Dimensions.get('window').width;


class Setting extends Component {

    constructor(props) {
        super(props);
        this.state = {
            deleteClick: false

        }
    }

   
    onLayout2open() {
        this.setState({ deleteClick: false })
        this.props.navigation.navigate('Delete')
    }

    async onSubmit() {

        await userProfileApi._other_details(this.state.user_id, this.state.createdBy, this.state.diet);
    }
    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#fafafa' }}>
                <View style={{ flex: 1 }}>
                    <StatusBar barStyle="light-content" backgroundColor={COLORS.appColor} />

                    <View style={{
                        height: 60,
                        backgroundColor: COLORS.appColor,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text
                            style={{
                                color: COLORS.appColor,
                                alignSelf: 'center',
                                alignItems: 'center',
                                fontSize: 22,
                                color: '#fff',
                            }
                            }>Setting</Text>
                    </View>

                    <View style={{ marginHorizontal: 40, marginTop: 40 }}>

                        <Pressable onPress={() => this.props.navigation.navigate('Delete')}>
                        <Text>Privacy Settings</Text>

                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />

                        <Pressable style={{ marginTop: 15 }} onPress={() => this.props.navigation.navigate('Delete')}>
                            <Text>Notification Settings</Text>
                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />

                        <Pressable style={{ marginTop: 15 }} onPress={() => this.props.navigation.navigate('Delete')}>
                            <Text>Change Password</Text>

                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />

                        <Pressable style={{ marginTop: 15 }} onPress={() => this.props.navigation.navigate('HideProfile')}>
                            <Text>Hide Profile</Text>

                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />
                        <Pressable style={{ marginTop: 15 }} onPress={() => this.setState({ deleteClick: true })}>
                            <Text>Delete Profile</Text>

                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />

                        <Pressable style={{ marginTop: 15 }} onPress={() => this.props.navigation.navigate('Delete')}>
                            <Text>Rate App</Text>
                        </Pressable>
                        <View style={{ marginTop: 8, borderBottom: 2, borderBottomColor: '#B5BBC5', borderBottomWidth: 2 }} />

                        <Pressable style={{
                            backgroundColor: '#d9475c',
                            justifyContent: 'center',
                            marginTop: 40,
                            paddingTop: 10,
                            paddingBottom: 10,
                            borderRadius: 30,
                            elevation: 5,
                            marginBottom: 20,
                            width: 100
                        }}
                        // onPress={() => this.props.navigation.navigate('')}
                        //  onPress={() => this.onSubmit()}
                        >
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    color: '#fff',
                                    fontSize: 12
                                }}
                            >Logout</Text>
                        </Pressable>

                    </View>


                    <Overlay isVisible={this.state.deleteClick}>
                        <View style={{ width: 350, paddingTop: 10, paddingBottom: 20, borderRadius: 15 }}>

                            <Text
                                style={{
                                    color: '#000',
                                    alignSelf: 'center',
                                    alignItems: 'center',
                                    fontSize: 20,
                                    marginTop: 30

                                }
                                }
                            >Are You Sure You Want to Delete Your Profile?</Text>

                            <View style={{ marginHorizontal: 20, marginTop: 10, flexDirection: 'row', justifyContent: 'space-between' }}>

                                <Pressable style={{
                                    backgroundColor: '#d9475c',
                                    justifyContent: 'center',
                                    marginTop: 40,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    borderRadius: 7,
                                    elevation: 5,
                                    marginHorizontal: 40,
                                    marginBottom: 20,
                                    width: 70
                                }}
                                    onPress={() => this.onLayout2open()}
                                >
                                    <Text
                                        style={{
                                            alignSelf: 'center',
                                            color: '#fff',
                                            fontSize: 15
                                        }}
                                    >Yes</Text>
                                </Pressable>

                                <Pressable style={{
                                    backgroundColor: '#d9475c',
                                    justifyContent: 'center',
                                    marginTop: 40,
                                    paddingTop: 10,
                                    paddingBottom: 10,
                                    borderRadius: 7,
                                    elevation: 5,
                                    marginHorizontal: 70,
                                    marginBottom: 20,
                                    width: 70
                                }}
                                    onPress={() => this.setState({ deleteClick: false })}
                                >
                                    <Text
                                        style={{
                                            alignSelf: 'center',
                                            color: '#fff',
                                            fontSize: 15
                                        }}
                                    >No</Text>
                                </Pressable>

                            </View>
                        </View>
                    </Overlay>

                </View>
            </ScrollView>
        );
    };
}



export default Setting;

const Styles = StyleSheet.create({
    dropDownStyles: {
        backgroundColor: '#fff',
        borderColor: '#000',
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        marginTop: 7,
        justifyContent: 'center'


    },
    phnDropDown: {
        backgroundColor: '#fff',


    }
})