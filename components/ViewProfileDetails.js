import * as React from 'react';
import { Text, View, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign'
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Dimensions } from 'react-native';
import { Pressable } from 'react-native';
import { COLORS } from '../styles/COLOR';
import Fontawesom from 'react-native-vector-icons/FontAwesome'
import Fontawesom5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Ionicon from 'react-native-vector-icons/Ionicons'
import { Component } from 'react';



const wv = Dimensions.get('window').width;


export default class ViewProfileDetails extends Component {

    render() {
        return (

            <ScrollView >
                <SafeAreaView style={[styles.page_background, { marginBottom: 40 }]}>
                    <View style={{ flex: 1 }}>
                        <Image
                            source={require('../images/girl.jpg')}
                            style={{
                                height: 250
                            }}
                        />
                        <View style={{
                            height: 600,
                            backgroundColor: '#f1f0f6',
                            marginTop: -40,
                            borderTopLeftRadius: 40,
                            borderTopRightRadius: 40
                        }}>



                            <View style={{ justifyContent: 'space-between', marginHorizontal: 30, flexDirection: 'row', marginBottom: 10, paddingTop: 12 }}>

                                <Pressable style={[styles.circleImgBg, { alignItems: 'center' }]}>
                                    <Fontawesom5 name='angle-left' size={22} color='#fff' />
                                </Pressable>

                                <Pressable style={[styles.circleImgBg, { alignItems: 'center' }]}>
                                    <MaterialIcons name='person-search' size={22} color='#fff' />
                                </Pressable>


                                <Pressable style={[styles.circleImgBg, { alignItems: 'center' }]}>
                                    <Fontawesom name='send' size={15} color='#fff' />
                                </Pressable>

                                <Pressable style={[styles.circleImgBg, { alignItems: 'center' }]}>
                                    <Ionicon name='chatbubble-ellipses-sharp' size={22} color='#fff' />
                                </Pressable>

                                <Pressable style={[styles.circleImgBg, { alignItems: 'center' }]}>
                                    <Fontawesom5 name='angle-right' size={22} color='#fff' />
                                </Pressable>


                            </View>



                            <Text style={{
                                marginLeft: 20,
                                marginTop: 25,
                                color: '#000000',
                                fontSize: 18
                            }}>User Name</Text>


                            <View
                                style={{
                                    flexDirection: 'row',
                                    marginLeft: 20,
                                    marginRight:30,
                                    marginTop: 10,
                                    justifyContent: 'space-between'
                                }}
                            >
                                <View

                                >

                                    <Text style={styles.textStyle}>20.5 T</Text>
                                    <Text style={styles.textStyle}>New Delhi</Text>
                                    <Text style={styles.textStyle}>Koi, koi,Mahadev</Text>
                                    <Text style={styles.textStyle}>Hindi,Delhi</Text>

                                </View>
                                <View>
                                    <Text style={styles.textStyle}>Trade School</Text>
                                    <Text style={styles.textStyle}>Medical / Healthcase Professional</Text>
                                    <Text style={styles.textStyle}>Rs. 8.3Lac</Text>
                                    <Text style={styles.textStyle}>Never married</Text>
                                </View>

                                

                            </View>

                            <View style={{borderBottomColor:'#b6b8ba',borderBottomWidth:.2,marginHorizontal:20,marginTop:18}}/>

                            <Text style={{
                                marginLeft: 20,
                                marginTop: 15,
                                color: '#000000',
                                fontSize: 18
                            }}>BIO</Text>

                            <Text style={[styles.textStyle,{marginHorizontal:20,textAlign:'justify'}]}>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry Lorem Ipsum has been the industry's standard dummy text ever since the 1500s when an unknown printer took a galley of type and scrambled it to make a type specimen book it has?

                            </Text>


                            

                        </View>


                    </View>

                </SafeAreaView >
            </ScrollView>
        );
    }
}


const styles = {
    page_background: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#f1f0f6',

    },

    MainContainer: {
        marginTop: 20
    },

    listcontainer: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        width: 150,
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginBottom: 10,
        paddingBottom: 20

    },

    button_design: {
        backgroundColor: '#fff',
        width: 300,
        height: hp('7%'),
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 30,
        marginBottom: 20,
        alignSelf: 'center'
    },

    small_button_design: {
        backgroundColor: '#fff',
        width: 150,
        height: hp('4%'),
        borderRadius: 6,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 8,
        marginTop: 20,
        marginBottom: 10,
        alignSelf: 'center'
    },

    bigger_card: {
        backgroundColor: '#FFF',
        flexDirection: 'column',
        flex: 1,
        flex_wrap: 'wrap',
        borderRadius: 15,
        marginRight: 10,
        marginLeft: 10,
        shadowColor: '#000',
        shadowOffset: { width: 2, height: 2 },
        shadowRadius: 6,
        shadowOpacity: 0.26,
        elevation: 10,
        marginBottom: 20,
        paddingBottom: 15,
        paddingTop: 10,
        marginTop: 20
    },

    design_image: {
        height: hp('18%'),
        width: 150,
        resizeMode: 'stretch',
        borderTopLeftRadius: 40,
        borderTopRightRadius: 40,
    },

    subHeading: {
        fontSize: 14,
        color: '#000',
        textAlign: 'left',
        marginTop: 20,
        marginStart: 10,
        fontWeight: 'bold'
    },
    profileDetailsText: {
        fontSize: 10,
        color: '#000',
        textAlign: 'left',
        marginStart: 5,
        marginTop: 5
    },
    circleImgBg: {
        backgroundColor: COLORS.appColor,
        marginEnd: 0,
        marginTop: -30,
        elevation: 10,
        height: 40,
        width: 40,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    circleImgBg_small: {
        backgroundColor: COLORS.appColor,
        marginEnd: 5,
        height: 25,
        width: 25,
        borderRadius: 400 / 2,
        justifyContent: 'center'
    },
    textStyle: {
        fontSize: 12,
        color:'#5a5b5c'
    }


}
