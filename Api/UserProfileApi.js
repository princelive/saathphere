import React, { Component } from "react";

import axios from 'axios';

import SecureStorage from 'react-native-secure-storage'
import color from "color";


const baseUrl = 'http://metromonial.itcomit.com/';
class User_Api extends Component {

    url = 'http://metromonial.itcomit.com/';

    _personal = async (user_id, bridge_name, dob, height, religion, community, marital_status, location, posted_by, mother_tounge) => {

        let token = await SecureStorage.getItem('token');

        // this.setState({spinner:true});
        //console.log('mother'+mother_tounge);
        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'personal');
        formData.append('user_id', user_id);
        formData.append('bridge_name', bridge_name);
        formData.append('dob', dob);
        formData.append('height', height);
        formData.append('religion', religion);
        formData.append('community', community);
        formData.append('marital_status', marital_status);
        formData.append('location', location);
        formData.append('posted_by', posted_by);
        formData.append('mother_tounge', mother_tounge);


        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _basicLifeStyle = async (user_id, age, dob, height, weight, grow_up, sun_sign, blood_group, health_info, disablity) => {

        let token = await SecureStorage.getItem('token');

        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'basic');
        formData.append('user_id', user_id);
        formData.append('age', age);
        formData.append('dob', dob);
        formData.append('height', height);
        formData.append('weight', weight);
        formData.append('grow_up', grow_up);
        formData.append('sun_sign', sun_sign);
        formData.append('blood_group', blood_group);
        formData.append('health_info', health_info);
        formData.append('disablity', disablity);


        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }


    _religion = async (user_id, religion, community, sub_community, gothro, mother_tongue, speak_language) => {

        let token = await SecureStorage.getItem('token');

        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'religion');
        formData.append('user_id', user_id);
        formData.append('religion', religion);
        formData.append('community', community);
        formData.append('sub_community', sub_community);
        formData.append('gothro', gothro);
        formData.append('mother_tongue', mother_tongue);
        formData.append('speak_language', speak_language);



        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _astroDetails = async (user_id, manglik, dob, tob, city_of_birth) => {

        let token = await SecureStorage.getItem('token');

        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'astro');
        formData.append('user_id', user_id);
        formData.append('manglik', manglik);
        formData.append('dob', dob);
        formData.append('tob', tob);
        formData.append('city_of_birth', city_of_birth);




        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }


    _familyDetails = async (user_id, father_status, mother_status, family_location, native_place, brothers, sisters, family_values, family_affluence) => {

        let token = await SecureStorage.getItem('token');

        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'family');
        formData.append('user_id', user_id);
        formData.append('mother_status', mother_status);
        formData.append('father_status', father_status);
        formData.append('family_location', family_location);
        formData.append('native_place', native_place);
        formData.append('brothers', brothers);
        formData.append('sisters', sisters);
        formData.append('family_affluence', family_affluence);
        formData.append('family_values', family_values);



        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }


    _education_career = async (user_id, highest_qualification, college_attented, annual_income, working_with, working_as, employer_name) => {

        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'education');
        formData.append('user_id', user_id);
        formData.append('highest_qualification', highest_qualification);
        formData.append('college_attented', college_attented);
        formData.append('annual_income', annual_income);
        formData.append('working_with', working_with);
        formData.append('working_as', working_as);
        formData.append('employer_name', employer_name);


        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _education_career_partner = async (user_id, highest_qualification, college_attented, annual_income, working_with, working_as, employer_name) => {

        // alert('hiii')
        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'partner_education');
        formData.append('user_id', user_id);
        formData.append('highest_qualification', highest_qualification);
        formData.append('college_attented', college_attented);
        formData.append('annual_income', annual_income);
        formData.append('working_with', working_with);
        formData.append('working_as', working_as);
        formData.append('employer_name', employer_name);


        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _other_details = async (user_id, profile_created_by,diet) => {

        // alert('hiii')
        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'partner_other');
        formData.append('user_id', user_id);
        formData.append('profile_created_by', profile_created_by);
        formData.append('diet', diet);
      
        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _locationAndGroom = async (user_id, current_city, state, residence_status, pin_code) => {

        // _locationAndGroom = async (formDataa) => {

        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = baseUrl + "api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'location');
        formData.append('user_id', user_id);
        formData.append('current_city', current_city);
        formData.append('state', state);
        formData.append('residence_status', residence_status);
        formData.append('pin_code', pin_code);



        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }


    _locationParter = async (user_id, country_living, state_living, city) => {

        // _locationAndGroom = async (formDataa) => {

        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        // console.log(user_id + "  " + country_living + "  " + state_living + "  " + city);

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        //var api = baseUrl + "api/complete-profile";
        var api = 'http://metromonial.itcomit.com/api/complete-profile';
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'partner_location');
        formData.append('user_id', user_id);
        formData.append('country_living', country_living);
        formData.append('state_living', state_living);
        formData.append('city', city);




        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    _partnerBasicInfo = async (user_id, age_range, height_range, religion, mother_tounge, marital_status) => {

        // _locationAndGroom = async (formDataa) => {

        let token = await SecureStorage.getItem('token');
        
        // console.log('user id ',user_id);
        // this.setState({spinner:true});
        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);

        var api = baseUrl + "api/complete-profile";
        //console.log("api+url"+api);
        const formData = new FormData();
        formData.append('detail_type', 'partner_basic');
        formData.append('user_id', user_id);
        formData.append('age_range', age_range);
        formData.append('height_range', height_range);
        formData.append('religion', religion);
        formData.append('mother_tounge', mother_tounge);
        formData.append('marital_status', marital_status);



        axios({
            url: api,
            method: 'POST',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })
    }



    async _motherTangues() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let motherTanguesList = [];

        var api = "http://metromonial.itcomit.com/api/mother-tangues";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    motherTanguesList.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return motherTanguesList;
    }


    async _countries() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let countries = [];

        var api = "http://metromonial.itcomit.com/api/countries";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                countries = response.data.response;

              
            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return countries;
    }



    async _state(id) {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let countries = [];

        var api = "http://metromonial.itcomit.com/api/states/" + id;
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                countries = response.data.response;

                //  list.forEach(element => {
                //     //console.log(element.name);
                //     countries.push(element.name);
                // });
                //console.log('aa',list);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return countries;
    }


    async _city(id) {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let cities = [];

        var api = "http://metromonial.itcomit.com/api/cities/" + id;
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                cities = response.data.response;

                //  list.forEach(element => {
                //     //console.log(element.name);
                //     countries.push(element.name);
                // });
                //console.log('aa',list);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return cities;
    }


    async _groomHeight() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let height = [];

        var api = "http://metromonial.itcomit.com/api/heights";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    height.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return height;
    }



    async _groomWeights() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let weights = [];

        var api = "http://metromonial.itcomit.com/api/weights";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;
                //console.log('weights ',list);
                list.forEach(element => {
                    //console.log(element.name)
                    weights.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return weights;
    }


    async _bloodGroups() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let bloodGroups = [];

        var api = "http://metromonial.itcomit.com/api/blood-groups";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    bloodGroups.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return bloodGroups;
    }


    async _complete_profile_categories() {


        let token = await SecureStorage.getItem('token');
        //console.log("api ",token);

        let categories = [];

        var api = "http://metromonial.itcomit.com/api/categories";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;
                //console.log("list ",list);
                list.forEach(element => {
                    //console.log(element.name);
                    categories.push(element.category);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return categories;
    }



    async _age() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let age = [];

        var api = "http://metromonial.itcomit.com/api/ages";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    age.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return age;
    }


    async _gotras() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let gotras = [];

        var api = "http://metromonial.itcomit.com/api/gotras";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    gotras.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return gotras;
    }



    async _familyValues() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let family_values = [];

        var api = "http://metromonial.itcomit.com/api/family-values";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    //console.log(element.name);
                    family_values.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return family_values;
    }

    async _familyInfluence() {


        let token = await SecureStorage.getItem('token');
        // console.log("api ",token);

        let family_affiliunce = [];

        var api = "http://metromonial.itcomit.com/api/family-affiliunce";
        //console.log("api+url"+api);
        const formData = new FormData();

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }

        })
            .then(function (response) {
                //this.setState({spinner:false});

                let list = response.data.response;

                list.forEach(element => {
                    // console.log(element.name);
                    family_affiliunce.push(element.name);
                });
                // console.log('aa',motherTanguesList);

                //  console.log("response :", response.data.response);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

        return family_affiliunce;
    }


    _profileStatus = async (profile) => {

        let token = await SecureStorage.getItem('token');
        // console.log('user id ',user_id);
        // this.setState({spinner:true});

        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/get-profiles/" + profile;

        let status='';

        await axios({
            url: api,
            method: 'GET',

            headers: { 'Authorization': 'Bearer ' + token }
        })
            .then(function (response) {
                //this.setState({spinner:false});

                console.log("response :", response.data.status);

                status = response.data.status;
                //console.log('res',status);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

            return status;
    }






}



const userApi = new User_Api();
export default userApi;