import React, { Component } from "react";
import SecureStorage from 'react-native-secure-storage'

import axios from 'axios';


class MembershipPlanApi extends Component {

    membershipPlan = async () => {

        let token = await SecureStorage.getItem('token');
        let result = {};
        var api = "http://metromonial.itcomit.com/api/plans";
    
        const formData = new FormData();
        
        await axios({
            url: api,
            method: 'GET',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
                result = response.data.response;
               console.log("response :", result);
            })
            .catch(function (error) {
                console.log("error from image : " + error);
            })

            return result;
    }     

}
const membershipPlanApi = new MembershipPlanApi();
export default membershipPlanApi;