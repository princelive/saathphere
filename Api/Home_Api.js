import React, { Component } from "react";
import SecureStorage from 'react-native-secure-storage'

import axios from 'axios';


class Home_Api extends Component {

    async banner_call() {
        let banner = [];
        await axios.post(' http://test-demo.in/evatv/api/banner.php?action=banner')
            .then(response => {
                //console.log("hellllllllllllllll");
                let bannerList = response.data.video_list;
                //console.log(bannerList);
                bannerList.forEach(element => {
                    //console.log(element.poster);
                    banner.push(element.poster);
                });
               // console.log('Api');

            });

        return banner;


    }

    async get_ListData() {
        let category_list = [];
        await axios.post('http://test-demo.in/evatv/api/category.php?action=category')
            .then(response => {
               
                category_list = response.data.category_list;
                // console.log(category_list.category);

               
                // console.log('Api');

            });

        return category_list;
    }


    getUserDetails = async () => {

        let token = await SecureStorage.getItem('token');
        let result = {};
        // this.setState({spinner:true});
        //console.log('mother'+mother_tounge);
        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = " http://metromonial.itcomit.com/api/get-profiles";
        // http://metromonial.itcomit.com/profile_images/

        const formData = new FormData();
        
        await axios({
            url: api,
            method: 'GET',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
                //this.setState({spinner:false});
                result = response.data.response;
                //console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

            return result;
    }

    recomendedCategory = async () => {
        let token = await SecureStorage.getItem('token');
        let result = {};
        // this.setState({spinner:true});
        //console.log('mother'+mother_tounge);
        //console.log("data "+userName+" "+e+" "+pass+" "+c_pass+" "+phn);
        var api = "http://metromonial.itcomit.com/api/categorize-profiles";
        // http://metromonial.itcomit.com/profile_images/

        const formData = new FormData();
        
        await axios({
            url: api,
            method: 'GET',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
                //this.setState({spinner:false});
                result = response.data.result;
                console.log("response :", response.data);

            })
            .catch(function (error) {
                //this.setState({spinner:false});
                console.log("error from image : " + error);
            })

            return result;
    }
   

  deleteProfile = async(comment) => {

        let token = await SecureStorage.getItem('token');
      
        var api = "http://metromonial.itcomit.com/api/delete-profile";
    
        const formData = new FormData();
        formData.append('message',comment);
    
        await axios({
            url: api,
            method: 'GET',
            data: formData,
            headers: { 'Authorization': 'Bearer ' + token }
        }).then(function (response) {
                result = response.data.response;
               console.log("response :", result.message);
            })
            .catch(function (error) {
                console.log("error from image : " + error);
            })
            }     

}
   

const homeApi = new Home_Api();
export default homeApi;